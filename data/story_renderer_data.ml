open Core

type raw_clause = Mk of string * raw_clause list

let rec pp_raw_clause fmt =
  let open Format in
  function
  | (Mk (ident, [])) -> Format.pp_print_string fmt ident
  | (Mk (ident, clauses)) ->
    Format.fprintf fmt "%s(%a)"
      ident
      (Format.pp_print_list
         ~pp_sep:(fun fmt () -> fprintf fmt ",@ ")
         pp_raw_clause
      ) clauses

let show_raw_clause = Format.asprintf "%a" pp_raw_clause


type clause = App of string * string list
[@@deriving equal, sexp]

let pp_clause fmt (App (fn, args)) =
  Format.fprintf fmt "%s(%a)" fn
    (Format.pp_print_list
       ~pp_sep:(fun fmt () ->
         Format.fprintf fmt ", ")
       Format.pp_print_string) args

type temporal_property = {
  offset: int;
  negated: bool;
  clause: clause;
}
[@@deriving equal, sexp]

let pp_temporal_property fmt {offset;negated;clause} =
  Format.fprintf fmt "%sholds(%a,%a)"
    (if negated then "not " else "")
    (fun fmt -> function 0 -> Format.fprintf fmt "T" | n -> Format.fprintf fmt "T + %d" n)
    offset
    pp_clause clause

type rule = {
  head: clause;
  constraints: clause list;
  preconditions: temporal_property list;
  initiated_fluents: clause list;
  terminated_fluents: clause list;  
}
[@@deriving equal, sexp]

let pp_fluent clause head fmt vl =
  Format.fprintf fmt {|
%s(T,%a,%a) :-
  possible(T,%a).
|} clause pp_clause head pp_clause vl pp_clause head

let pp_rule fmt {head;constraints;preconditions;initiated_fluents;terminated_fluents} =
  let constraints = App ("time", ["T"]) :: constraints in
  Format.fprintf fmt {|
possible(T,%a) :- %a%s%a.

%a
%a
|} pp_clause head
    (Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ") pp_temporal_property)
    preconditions
    (match preconditions,constraints with (_ :: _), (_ :: _) -> ", " | _ -> "")
    (Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt ",@ ") pp_clause)
    constraints
    (Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt "\n") (pp_fluent "initiates" head))
    initiated_fluents
    (Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt "\n") (pp_fluent "terminates" head))
    terminated_fluents

type scenario = {
  name: string;
  temporal_clauses: (int * bool * clause) list;
  constant_clauses: clause list
}
[@@deriving equal, sexp]

let pp_constant_temporal_clause fmt (i,happens,cl) =
  if happens
  then
    Format.fprintf fmt ":- not happens(%d,%a)." i pp_clause cl
  else
    Format.fprintf fmt "holds(%d,%a)." i pp_clause cl

let pp_constant_clause fmt cl =
  Format.fprintf fmt "%a." pp_clause cl

let pp_scenario
      fmt {name=_;temporal_clauses; constant_clauses} =
  Format.fprintf fmt "%a\n%a"
    (Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt "\n") pp_constant_temporal_clause)
    temporal_clauses
    (Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt "\n") pp_constant_clause)
    constant_clauses

type problem_spec = {
  rules: rule list;
  scenario: scenario;
  max_time: int;
}
[@@deriving equal, sexp, show]

type event = {
  start: int option;
  end_: int option;
  clause: clause;
}
[@@deriving equal, sexp, show]

type model = {
  intervals: event list;
  events: (int * clause) list;
}
[@@deriving equal, sexp, show]

module Clingo = struct

  let time_prelude ~max_time () = Format.sprintf {|
time(0..%d).
|} max_time

  let ec_prelude = {|

{ happens(T,E) } :- time(T), possible(T,E).

:- time(T), not { happens(T,E) } = 1.

initiated(T,F) :- happens(T,E), initiates(T,E,F).
holds(T+1,F) :- time(T), happens(T,E), initiates(T,E,F).

terminated(T,F) :- happens(T,E), terminates(T,E,F).
holds(T+1,F) :- time(T), holds(T,F), not terminated(T,F).
|}

  let ec_postlude = {|

#show happens/2.

#show initiated/2.

#show terminated/2.

|}



  let make_clingo_input ({
    rules; scenario; max_time
  } : problem_spec) =
    Format.asprintf {|
%s
%s

%a

%a   

%s
|}
      (time_prelude ~max_time ())
      ec_prelude
      pp_scenario scenario
      (Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt "\n")
         pp_rule) rules
      ec_postlude

end
