[@@@warning "-32"]
module T = (Testing_utils.Lwt.Make (struct
                  let name = "serialisation"
                end))

module Data = Story_renderer_data
module SR = Story_renderer
module SRO = SR.Operations

let clause =
  Alcotest.testable Data.pp_clause
    Data.equal_clause

let rule =
  Alcotest.testable Data.pp_rule
    Data.equal_rule

let scenario =
  Alcotest.testable Data.pp_scenario
    Data.equal_scenario

let temporal_property =
  Alcotest.testable Data.pp_temporal_property
    Data.equal_temporal_property

let problem_spec =
  Alcotest.testable Data.pp_problem_spec
    Data.equal_problem_spec

let () = T.add_result_test "serialises empty without error" (fun () ->
    let open Lwt_result.Let_syntax in
    let%bind _ = Story_renderer.Serialisation.serialise Data.{
        rules=[{
            head=App ("f1", ["x1"; "x2"]);
            constraints=[];
            preconditions=[];
            initiated_fluents=[];
            terminated_fluents=[]
          }];
        scenario={
          name="temp";
          temporal_clauses=[];
          constant_clauses=[];
        };
        max_time=10;
      } in
    Lwt_result.return ())

let () =
  let data = Data.{
      rules=[{
          head=App ("f1", ["x1"; "x2"]);
          constraints=[];
          preconditions=[];
          initiated_fluents=[];
          terminated_fluents=[]
        }];
      scenario={
        name="temp";
        temporal_clauses=[];
        constant_clauses=[];
      };
      max_time=10;
    } in
  T.add_result_test "serialises and deserialises empty without error"
    (fun () ->
       let open Lwt_result.Let_syntax in
       let%bind binary = Story_renderer.Serialisation.serialise data in
       let%bind data' = Story_renderer.Serialisation.deserialise binary in
       Alcotest.check problem_spec "deserialised data is equal" data' data;
       Lwt_result.return ())

let () =
  let data = Data.{
      rules=[{
          head=App ("f1", ["x1"; "x2"]);
          constraints=[App ("f2", ["x3"; "x4"])];
          preconditions=[
            { offset=0; negated=false; clause=App ("f3", ["x5"; "x6"]) };
            { offset=2; negated=true; clause=App ("f4", ["x7"; "x8"]) }
          ];
          initiated_fluents=[
            App ("f1", ["x1"; "x2"]);
            App ("f2", ["x2"; "x3"]);
            App ("f3", ["x3"; "x4"])
          ];
          terminated_fluents=[
            App ("f5", ["x9"; "x10"]);
            App ("f6", ["x11"; "x12"]);
          ]
        };
         {
           head=App ("f12", ["x1"; "x2"]);
           constraints=[App ("f3", ["x3"; "x4"])];
           preconditions=[
             { offset=0; negated=false; clause=App ("f4", ["x5"; "x6"]) };
             { offset=2; negated=true; clause=App ("f5", ["x7"; "x8"]) }
           ];
           initiated_fluents=[
             App ("f1", ["x1"; "x2"]);
             App ("f2", ["x2"; "x3"]);
             App ("f3", ["x3"; "x4"])
           ];
           terminated_fluents=[
             App ("f5", ["x9"; "x10"]);
             App ("f6", ["x11"; "x12"]);
           ]
         }
        ];
      scenario={
        name="temp";
        temporal_clauses=[
            0, App ("f5", ["x9"; "x10"]);
            10, App ("f6", ["x11"; "x12"]);
        ];
        constant_clauses=[
            App ("f5", ["x9"; "x10"]);
            App ("f6", ["x11"; "x12"]);
        ];
      };
      max_time=10;
    } in
  T.add_result_test "serialises and deserialises complex without error"
    (fun () ->
       let open Lwt_result.Let_syntax in
       let%bind binary = Story_renderer.Serialisation.serialise data in
       let%bind data' = Story_renderer.Serialisation.deserialise binary in
       Alcotest.check problem_spec "deserialised data is equal" data' data;
       Lwt_result.return ())

let () = T.run ()
