[@@@warning "-32"]
module T = (Testing_utils.Lwt.Make (struct
                  let name = "sql"
                end))
module Data = Story_renderer_data
module SR = Story_renderer
module SRO = SR.Operations

let clause =
  Alcotest.testable Data.pp_clause
    Data.equal_clause

let rule =
  Alcotest.testable Data.pp_rule
    Data.equal_rule

let scenario =
  Alcotest.testable Data.pp_scenario
    Data.equal_scenario

let scenario =
  Alcotest.testable Data.pp_temporal_property
    Data.equal_temporal_property

let sanitize_error err =
  Lwt_result.map_error (fun e ->
    `Msg (Caqti_error.show e)) err

let init_mem_conn () =
  Caqti_lwt.connect
    (Uri.of_string "sqlite3::memory:")

let with_db f () = 
  let open Lwt_result.Let_syntax in
  let%bind db = init_mem_conn () |> sanitize_error in
  let%bind () = Sql.initialise db in
  f db

let () =
  let open Lwt_result.Let_syntax in
  T.add_result_test "adding clause" @@ with_db @@
  fun db -> 
  let%bind i1 =
    SRO.ensure_clause_instance
      (Data.App ("reachable", ["exegol";"panama"]))
      db in
  let%bind Data.App (f, args) =
    SRO.resolve_instance i1 db in
  Alcotest.(check string) "function equality"
    "reachable" f;
  Alcotest.(check (list string)) "function args"
    ["exegol"; "panama"] args;
  Lwt.return_ok ()

let () =
  let open Lwt_result.Let_syntax in
  T.add_result_test "adding multiple clauses" @@ with_db @@
  fun db ->
  let l1 = Data.App ("reachable", ["exegol";"panama"]) in
  let l2 = Data.App ("reachable", ["panama";"exegol"]) in
  let l3 = Data.App ("reachable", ["panama";"flywheel"]) in
  let l4 = Data.App ("reachable", ["panama";"exedos"]) in

  let%bind i1 = SRO.ensure_clause_instance l1 db in
  let%bind i2 = SRO.ensure_clause_instance l2 db in
  let%bind i3 = SRO.ensure_clause_instance l3 db in
  let%bind i4 = SRO.ensure_clause_instance l4 db in

  let%bind l1' = SRO.resolve_instance i1 db in
  let%bind l2' = SRO.resolve_instance i2 db in
  let%bind l3' = SRO.resolve_instance i3 db in
  let%bind l4' = SRO.resolve_instance i4 db in

  Alcotest.(check clause) "clause equality" l1' l1;
  Alcotest.(check clause) "clause equality" l2' l2;
  Alcotest.(check clause) "clause equality" l3' l3;
  Alcotest.(check clause) "clause equality" l4' l4;

  Lwt.return_ok ()

let () =
  let open Lwt_result.Let_syntax in
  T.add_result_test "adding rule" @@ with_db @@
  fun db ->
  let rle = Data.({
    head=App ("reachable", ["FP";"TP"]);
    constraints=[App ("place", ["FP"]); App ("place", ["TP"])];
    preconditions=[{offset=0;negated=true;clause=App ("destroyed", ["TP"])}];
    initiated_fluents=[App("flows",["FP"])];
    terminated_fluents=[App("flows",["TP"])];
  }) in

  let%bind rlink = SRO.insert_rule rle db in

  let%bind rle' = SRO.resolve_rule rlink db in

  Alcotest.(check rule) "rule equality" rle rle';

  Lwt.return_ok ()


let () = T.run ()
