open Core

type (_,_) eq = Refl: ('a,'a) eq

type table_name = int

type 'a ty =
  | BOOL : bool ty
  | INTEGER : int ty
  | REAL: float ty
  | TEXT : string ty
  | BLOB : string ty

  | NULLABLE_BOOL: bool option ty
  | NULLABLE_INTEGER: int option ty
  | NULLABLE_REAL: float option ty
  | NULLABLE_TEXT: string option ty
  | NULLABLE_BLOB: string option ty

type _ ty_list =
  | Nil : unit ty_list
  | Cons : 'a ty * 'b ty_list -> ('a * 'b) ty_list

type conflict_clause = [`ROLLBACK | `ABORT | `FAIL | `IGNORE | `REPLACE]
type foreign_conflict_clause = [`SET_NULL | `SET_DEFAULT | `CASCADE | `RESTRICT | `NO_ACTION ]
type 'a sql_constraint =
  | PrimaryKey of {
    name: string option;
    ordering: [`ASC | `DESC] option;
    local_columns: string list option;
    on_conflict: conflict_clause option;
    auto_increment: bool
  }
  | NotNull of {
      name: string option;
      on_conflict: conflict_clause option
    }
  | Unique of {
      name: string option;
      local_columns: string list option;
      on_conflict: conflict_clause option;
    }
  | ForeignKey of {
      local_columns: string list option;
      name: string option;
      table: table_name;
      columns: string list;
      on_update: foreign_conflict_clause option;
      on_delete: foreign_conflict_clause option;
    }

type 'a schema_field = string * 'a ty * [`Column] sql_constraint list


module Type = struct

  type 'a t = 'a ty

  let bool = BOOL
  let int = INTEGER
  let real = REAL
  let text = TEXT
  let blob = BLOB

  let null_ty: 'a . 'a ty -> 'a option ty =
    fun (type a) (ty: a ty) : a option ty ->
    match ty with
    | BOOL      -> NULLABLE_BOOL   
    | INTEGER   -> NULLABLE_INTEGER
    | REAL      -> NULLABLE_REAL   
    | TEXT      -> NULLABLE_TEXT   
    | BLOB      -> NULLABLE_BLOB   
    | _ -> invalid_arg "can not null a nullable type"

  let ty_to_caqti_ty: 'a . 'a t -> 'a Caqti_type.t =
    fun (type a) (ty: a ty) : a Caqti_type.t ->
    match ty with
    | BOOL -> Caqti_type.bool
    | INTEGER -> Caqti_type.int
    | REAL -> Caqti_type.float
    | TEXT -> Caqti_type.string
    | BLOB -> Caqti_type.string

    | NULLABLE_BOOL -> Caqti_type.option Caqti_type.bool
    | NULLABLE_INTEGER -> Caqti_type.option Caqti_type.int
    | NULLABLE_REAL -> Caqti_type.option Caqti_type.float
    | NULLABLE_TEXT -> Caqti_type.option Caqti_type.string
    | NULLABLE_BLOB -> Caqti_type.option Caqti_type.string

  let rec ty_list_to_caqti_ty: 'a . 'a ty_list -> 'a Caqti_type.t =
    fun (type a) (ls: a ty_list) : a Caqti_type.t ->
    match ls with
    | Nil -> Caqti_type.unit
    | Cons (h, t) ->
      Caqti_type.tup2 (ty_to_caqti_ty h) (ty_list_to_caqti_ty t)

  let eq_ty: 'a 'b . 'a t * 'b t -> ('a,'b) eq option =
    fun (type a b) ((e1,e2): a ty * b ty) : (a,b) eq option ->
    match e1,e2 with
    | (BOOL, BOOL) -> Some Refl
    | (INTEGER, INTEGER) -> Some Refl
    | (REAL, REAL) -> Some Refl
    | (TEXT, TEXT) -> Some Refl
    | (BLOB, BLOB) -> Some Refl
    | _ -> None

  let rec eq_ty_list : 'a 'b . 'a ty_list * 'b ty_list -> ('a,'b) eq option =
    fun (type a b) ((l1,l2): (a ty_list * b ty_list)) : (a,b) eq option ->
    match l1,l2 with
    | Nil, Nil -> Some Refl
    | Cons (h1,t1), Cons (h2, t2) -> begin
        match eq_ty (h1,h2) with
        | Some Refl -> begin match eq_ty_list (t1, t2) with
          | Some Refl -> Some Refl
          | None -> None
        end
        | None -> None
      end
    | _ -> None

  let show : 'a . 'a t -> string =  fun (type a) (ty: a t) ->
    match ty with
    | BOOL | INTEGER -> "INTEGER"
    | REAL -> "REAL"
    | TEXT -> "TEXT"
    | BLOB -> "BLOB"

    | NULLABLE_BOOL | NULLABLE_INTEGER -> "INTEGER"
    | NULLABLE_REAL -> "REAL"
    | NULLABLE_TEXT -> "TEXT"
    | NULLABLE_BLOB -> "BLOB"

  module Map (S: sig type ('key,'vl) t end) : sig
    type !'a t
    val empty: 'a t

    val lookup_opt : 'opt t -> key:'k ty_list -> ('k, 'opt) S.t option

    val insert : 'a t -> key:'k ty_list -> data:('k, 'a) S.t -> 'a t
  end = struct

    type !'a t =
      | [] : 'a t
      | (::) : ('k ty_list * ('k, 'a) S.t) * 'a t -> 'a t

    let empty : 'a t = []

    let rec lookup_opt: 'a t -> key:'k ty_list -> ('k,'a) S.t option =
      let handle: 'a 'b . 'b ty_list -> 'a ty_list * ('a, 'opt) S.t -> ('b, 'opt) S.t option =
        fun (type a b) (l1: b ty_list) ((l2,data): (a ty_list * (a, 'opt) S.t)) : (b, 'opt) S.t option ->
          match eq_ty_list (l1, l2) with
          | Some Refl -> Some data
          | _ -> None in
      fun ls ~key ->
        match ls with
        | [] -> None
        | data :: t ->
          match handle key data with
          | Some data -> Some data
          | None -> lookup_opt t ~key

    let insert : 'a t -> key:'k ty_list -> data:('k,'a) S.t -> 'a t =
      fun ls ~key ~data -> (key,data) :: ls
  end

end


type 'a field = table_name * 'a schema_field

type comparison = EQ | NEQ
                | GT | GE
                | LT | LE

type join_op = LEFT | RIGHT | INNER 

let pp_join_op fmt = function
  | LEFT -> Format.fprintf fmt "LEFT JOIN"
  | RIGHT -> Format.fprintf fmt "RIGHT JOIN"
  | INNER -> Format.fprintf fmt "INNER JOIN"

type 'a expr_list =
  | [] : unit expr_list
  | (::) : ('a expr * 'b expr_list) -> ('a * 'b) expr_list
  (* | App : ('a expr_list * 'b expr_list) -> ('a * 'b) expr_list *)

and 'a expr =
  | ADD : int expr * int expr -> int expr
  | SUB : int expr * int expr -> int expr

  | COMPARE : comparison * 'a expr * 'a expr -> bool expr
  | AND : bool expr * bool expr -> bool expr
  | OR : bool expr * bool expr -> bool expr
  | IS_NOT_NULL: 'a expr -> bool expr
  | FIELD : 'a field -> 'a expr
  | CONST : 'a * 'a ty -> 'a expr
  | CONST_STATIC : 'a * 'a ty -> 'a expr
  | COERCETO: 'a expr * 'b ty -> 'b expr
  | AS: 'a expr * string -> 'a expr
  | REF : string * 'a ty -> 'a expr
  | NULLABLE: 'a expr -> 'a option expr

  | COUNT: bool * 'b expr_list -> int expr
  | COUNT_STAR: int expr
  | MAX: bool * int expr -> int expr
  | MIN: bool * int expr -> int expr
  | SUM: bool * int expr -> int expr
  | TOTAL: bool * int expr -> int expr
  | GROUP_CONCAT: bool * string expr * string expr option -> string expr

  | ABS : int expr -> int expr
  | CHANGES: int expr
  | GLOB: string expr * string expr -> bool expr
  | COALESCE: 'a expr list -> 'a expr
  | LIKE: string expr * string expr -> bool expr
  | MIN_OF: int expr list -> int expr
  | MAX_OF: int expr list -> int expr
  | RANDOM: int expr
  | LOWER: string expr -> string expr
  | UPPER: string expr -> string expr
and wrapped_assign = ASSIGN : 'a expr * 'a expr -> wrapped_assign
and (_, !'res) query =
  | SELECT_CORE : {
    exprs: 'a expr_list;
    table: table_name;
    join: join list;
    where: bool expr option;
    group_by: 'b expr_list option;
    having: bool expr option;
  } -> ('a, [> `SELECT_CORE] as 'res) query
  | SELECT : {
      core: ('a, [< `SELECT_CORE ]) query;
      order_by: ([`ASC | `DESC] * 'e expr) option;
      limit: int expr option;
      offset: int expr option
    } -> ('a, [> `SELECT] as 'res) query
  | DELETE : {
      table: table_name;
      where: bool expr option;
    } -> (unit, [> `DELETE] as 'res) query
  | UPDATE : {
      table: table_name;
      on_err: [`ABORT | `FAIL | `IGNORE | `REPLACE | `ROLLBACK ] option;
      set: wrapped_assign list;
      where: bool expr option;
    } -> (unit, [> `UPDATE] as 'res) query
  | INSERT : {
      table: table_name;
      on_err: [`ABORT | `FAIL | `IGNORE | `REPLACE | `ROLLBACK ] option;
      set: wrapped_assign list;
    } -> (unit, [> `INSERT] as 'res) query

and join = MkJoin: {
  table: ('r, [< `SELECT_CORE | `SELECT ]) query;
  on: bool expr;
  join_op: join_op;
} -> join


module Schema = struct

  type conflict_clause = [`ROLLBACK | `ABORT | `FAIL | `IGNORE | `REPLACE]
  type foreign_conflict_clause = [`SET_NULL | `SET_DEFAULT | `CASCADE | `RESTRICT | `NO_ACTION ]

  type 'a field = 'a schema_field
  type 'a constraint_ = 'a sql_constraint

  let pp_conflict_clause fmt = function
    | `ROLLBACK ->
      Format.fprintf fmt "ON CONFLICT ROLLBACK"
    | `ABORT ->
      Format.fprintf fmt "ON CONFLICT ABORT"
    | `FAIL ->
      Format.fprintf fmt "ON CONFLICT FAIL"
    | `IGNORE ->
      Format.fprintf fmt "ON CONFLICT IGNORE"
    | `REPLACE ->
      Format.fprintf fmt "ON CONFLICT REPLACE"


  let pp_foreign_conflict_clause fmt = function
    | `SET_NULL -> Format.fprintf fmt "SET NULL"
    | `SET_DEFAULT -> Format.fprintf fmt "SET DEFAULT"
    | `CASCADE -> Format.fprintf fmt "CASCADE"
    | `RESTRICT -> Format.fprintf fmt "RESTRICT"
    | `NO_ACTION -> Format.fprintf fmt "NO ACTION"


  let pp_opt f fmt = function
      None -> ()
    | Some v -> Format.fprintf fmt " %a" f v
  let pp_parens f fmt = fun v ->
    Format.fprintf fmt "(%a)" f v

  let pp_ordering fmt = function
    | `ASC -> Format.fprintf fmt "ASC"
    | `DESC -> Format.fprintf fmt "DESC"

  let pp_column_list fmt ls =
    Format.pp_print_list
      ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
      Format.pp_print_string fmt ls

  let pp_constraint_name fmt = function
    | None -> ()
    | Some name ->
      Format.fprintf fmt "CONSTRAINT %s " name

  let pp_sql_constraint resolve_table_name fmt = function
    | PrimaryKey {
      name; ordering; local_columns;
      on_conflict; auto_increment;
    } ->
      Format.fprintf fmt
        "%aPRIMARY KEY%a%a%a%a"
        pp_constraint_name name
        (pp_opt pp_ordering) ordering
        (pp_opt (pp_parens pp_column_list)) local_columns
        (pp_opt pp_conflict_clause) on_conflict
        (fun fmt vl ->
           if vl then
             Format.fprintf fmt " AUTOINCREMENT")
        auto_increment
    | NotNull { name; on_conflict } ->
      Format.fprintf fmt
        "%aNOT NULL%a"
        pp_constraint_name name
        (pp_opt pp_conflict_clause) on_conflict
    | Unique { name; local_columns; on_conflict } ->
      Format.fprintf fmt
        "%aUNIQUE%a%a"
        pp_constraint_name name
        (pp_opt (pp_parens pp_column_list)) local_columns
        (pp_opt pp_conflict_clause) on_conflict
    | ForeignKey {
      local_columns;
      name;
      table;
      columns;
      on_update;
      on_delete
    } ->
      Format.fprintf fmt
        "%a%a%sREFERENCES %s %a%a%a"
        pp_constraint_name name
        (pp_opt (fun fmt vl ->
           Format.fprintf fmt
             "FOREIGN KEY %a"
             (pp_parens pp_column_list)
             vl)) local_columns
        (if Option.is_some name || Option.is_some local_columns then " " else "")
        (resolve_table_name table)
        (pp_parens pp_column_list) columns
        (pp_opt (fun fmt vl ->
           Format.fprintf fmt "ON UPDATE %a"
             pp_foreign_conflict_clause vl)) on_update
        (pp_opt (fun fmt vl ->
           Format.fprintf fmt "ON DELETE %a"
             pp_foreign_conflict_clause vl)) on_delete

  let ensure_table_constraint : 'a sql_constraint -> unit =
    function
    | NotNull _ ->
      invalid_arg "NOT NULL constraints are not table constraints"
    | PrimaryKey {
      name=_; ordering; local_columns; on_conflict=_;
      auto_increment } ->
      if auto_increment then
        invalid_arg "PRIMARY KEY constraints when given as a table \
                     constraint can not auto-increment";
      if Option.is_some ordering then
        invalid_arg "PRIMARY KEY constraints when given as a table \
                     constraint can not specify ordering";
      if Option.is_none local_columns then
        invalid_arg "PRIMARY KEY constraints when given as a table \
                     constraint must specify columns explicitly";
    | Unique { name=_; local_columns; on_conflict=_ } ->
      if Option.is_none local_columns then
        invalid_arg "UNIQUE constraints when given as a table \
                     constraint must specify columns explicitly";
    | ForeignKey { local_columns; name=_; table=_; columns=_;
                   on_update=_; on_delete=_ } ->
      if Option.is_none local_columns then
        invalid_arg "UNIQUE constraints when given as a table \
                     constraint must specify the local columns \
                     explicitly"

  let ensure_column_constraint : 'a sql_constraint -> unit =
    function
    | NotNull _ -> ()
    | PrimaryKey {
      name=_; ordering=_; local_columns; on_conflict=_;
      auto_increment=_ } ->
      if Option.is_some local_columns then
        invalid_arg "PRIMARY KEY column constraints can not list \
                     columns explicitly";
    | Unique { name=_; local_columns; on_conflict=_ } ->
      if Option.is_some local_columns then
        invalid_arg "UNIQUE column constraints can not list \
                     columns explicitly";
    | ForeignKey { local_columns; name=_; table=_; columns=_;
                   on_update=_; on_delete=_ } ->
      if Option.is_some local_columns then
        invalid_arg "FOREIGN KEY column constraints can not list \
                     columns explicitly"

  let field ?(constraints : _ list =[]) name ~ty : 'a field =
    List.iter ~f:ensure_column_constraint constraints;
    (name, ty, constraints)

  let field_name = function (name, _, _) -> name

  let primary_key
        ?name ?ordering ?on_conflict
        ?(auto_increment=false) () : [`Column] sql_constraint =
    PrimaryKey {
      name;
      ordering;
      local_columns=None;
      on_conflict; auto_increment
    }

  let table_primary_key
        ?name ?on_conflict
        columns : [`Table] sql_constraint =
    PrimaryKey {
      name;
      ordering=None;
      local_columns=Some columns;
      on_conflict;
      auto_increment=false
    }

  let not_null ?name ?on_conflict () : [`Column] sql_constraint =
    NotNull { name; on_conflict }

  let unique ?name ?on_conflict () : [`Column] sql_constraint =
    Unique {
      name;
      local_columns=None;
      on_conflict;
    }

  let table_unique ?name ?on_conflict columns : [`Table] sql_constraint =
    Unique {
      name;
      local_columns=Some columns;
      on_conflict;
    }

  let rec expr_list_to_column_names : 'a . table_name -> 'a expr_list -> string list =
    fun (type a) table_name (ls: a expr_list) : string list ->
    match ls with
    | [] -> []
    | FIELD (table_name', (name, _, _)) :: t ->
      if not (Int.equal table_name table_name') then
        invalid_arg "foreign key constraint uses fields from a \
                     different table than the one specified";
      name :: expr_list_to_column_names table_name t
    | _ :: _ ->
      invalid_arg "foreign key constraint must operate on fields \
                    directly not derived expressions" 

  let foreign_key ?name ?on_update ?on_delete ~table ~columns () : [`Column] sql_constraint =
    ForeignKey {
      local_columns=None;
      name;
      table;
      columns=expr_list_to_column_names table columns;
      on_update;
      on_delete;
    }

  let table_foreign_key ?name ?on_update ?on_delete ~table ~columns local_columns : [`Table] sql_constraint =
    ForeignKey {
      local_columns=Some local_columns;
      name;
      table;
      columns=expr_list_to_column_names table columns;
      on_update;
      on_delete;
    }


  let ty : 'a field -> 'a ty = function (_, ty, _) -> ty

  type 'a table =
    | [] : unit table
    | (::) : ('a field * 'b table) -> ('a * 'b) table

  let to_sql ~resolve_table_name ~name table (constraints: 'a sql_constraint list) = 
    let rec loop  : 'a . string option -> 'a table -> string option = fun acc (type a) (table: a table) ->
      let update_acc_with acc res =
        let acc = match acc with None -> "" | Some v -> v ^ ", "  in
        let acc = acc ^ res in
        Some acc in
      match table with
      | [] -> acc
      | ((f, ty, constraints) :: rest) ->
        let constraints_text =
          if List.is_empty constraints then ""
          else Format.asprintf " %a"
                 (Format.pp_print_list ~pp_sep:Format.pp_print_space (pp_sql_constraint resolve_table_name))
                 constraints in
        let acc = update_acc_with acc @@ f ^ " " ^ Type.show ty ^ constraints_text in
        loop acc rest in
    let acc = (loop None table) in
    let acc = match acc,constraints with
      | None, [] -> ""
      | Some acc, [] -> acc
      | None, constraints ->
        Format.asprintf "%a"
          (Format.pp_print_list
             ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
             (pp_sql_constraint resolve_table_name)) constraints
      | Some acc, constraints ->
        Format.asprintf "%s, %a" acc
          (Format.pp_print_list
             ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
             (pp_sql_constraint resolve_table_name)) constraints in
    Format.sprintf "CREATE TABLE IF NOT EXISTS %s (%s)"
      name acc

end


type wrapped_table = MkTable : int * string * 'a Schema.table * [ `Table ] Schema.constraint_ list -> wrapped_table

type ('a,'c) filter_fun =
  bool expr -> ('c, 'a) query
  -> ('c, 'a) query
  constraint 'a =
    ([< `SELECT_CORE | `SELECT | `DELETE | `UPDATE]) as 'a

type ('a,'b,'c) group_by_fun =
  'b expr_list -> ('c, 'a) query -> ('c, 'a) query constraint 'a = ([< `SELECT_CORE | `SELECT ] as 'a)

type ('a,'c) having_fun =
  bool expr -> ('c, 'a) query
  -> ('c, 'a) query
  constraint 'a =
    ([< `SELECT_CORE | `SELECT ]) as 'a

type ('a,'b,'d,'c) join_fun =
  ?op:join_op -> on:bool expr ->
  ('b, [< `SELECT_CORE | `SELECT ] as 'd) query
  -> ('c, 'a) query -> ('c, 'a) query
  constraint 'a = ([< `SELECT_CORE]) as 'a

type ('a,'b,'c) on_err_fun =
  ([< `ABORT | `FAIL | `IGNORE | `REPLACE | `ROLLBACK ] as 'b) ->
  ('c, 'a) query
  -> ('c, 'a) query
  constraint 'a = ([> `UPDATE | `INSERT]) as 'a

let tables = Hashtbl.create (module Int)

let resolve_table_name : table_name -> string =
  fun name ->
  let MkTable (_, name, _, _) = Hashtbl.find_exn tables name in
  name

let declare_table ?(constraints : _ list =[]) ~name tbl =
  List.iter ~f:Schema.ensure_table_constraint constraints;
  let id = Hashtbl.length tables in
  Hashtbl.add_exn ~key:id ~data:(MkTable (id, name, tbl, constraints)) tables;
  let rec to_table : 'a . string -> 'a Schema.table -> 'a expr_list = fun name (type a) (table: a Schema.table) : a expr_list ->
    match table with
    | [] -> []
    | field :: rest -> ((FIELD (id,field)) : _ expr) :: to_table name rest in
  let table = to_table name tbl in
  id, table

let initialise (module DB: Caqti_lwt.CONNECTION) =
  let open Lwt_result.Let_syntax in
  let table_defs =
    Hashtbl.fold ~init:([]: 'a list)
      ~f:(fun ~key:_
           ~data:(MkTable (_, name, table, constraints)) acc ->
           List.cons (Schema.to_sql ~resolve_table_name ~name table constraints) acc
         ) tables in
  let%bind _ = 
    Lwt_list.map_s (fun table_def ->
      let req = Caqti_request.Infix.(Caqti_type.unit ->. Caqti_type.unit) table_def in
      DB.exec req () |> Lwt_result.map_error (fun err -> `Msg (Caqti_error.show err))
    ) table_defs
    |> Lwt.map (Result.all) in
  Lwt_result.return ()

module Expr = struct
  type 'a t = 'a expr

  type wrapped_value = MkWrapped: 'a ty * 'a -> wrapped_value

  let pp_wrapped_value fmt = function
    | MkWrapped (BOOL, b) -> Format.fprintf fmt "(BOOL,%b)" b
    | MkWrapped (INTEGER, i) -> Format.fprintf fmt "(INTEGER,%d)" i
    | MkWrapped (REAL, f) -> Format.fprintf fmt "(REAL,%f)" f
    | MkWrapped (TEXT, t) -> Format.fprintf fmt "(TEXT,'%s')" t
    | MkWrapped (BLOB, t) -> Format.fprintf fmt "(BLOB,'%s')" t

    | MkWrapped (NULLABLE_BOOL, None) -> Format.fprintf fmt "(BOOL,NULL)"
    | MkWrapped (NULLABLE_BOOL, Some b) -> Format.fprintf fmt "(BOOL,%b)" b
    | MkWrapped (NULLABLE_INTEGER, None) -> Format.fprintf fmt "(INTEGER,NULL)"
    | MkWrapped (NULLABLE_INTEGER, Some i) -> Format.fprintf fmt "(INTEGER,%d)" i
    | MkWrapped (NULLABLE_REAL, None) -> Format.fprintf fmt "(REAL,NULL)"
    | MkWrapped (NULLABLE_REAL, Some f) -> Format.fprintf fmt "(REAL,%f)" f
    | MkWrapped (NULLABLE_TEXT, None) -> Format.fprintf fmt "(TEXT,'NULL)"
    | MkWrapped (NULLABLE_TEXT, Some t) -> Format.fprintf fmt "(TEXT,'%s')" t
    | MkWrapped (NULLABLE_BLOB, None) -> Format.fprintf fmt "(BLOB,'NULL)"
    | MkWrapped (NULLABLE_BLOB, Some t) -> Format.fprintf fmt "(BLOB,'%s')" t

  [@@warning "-32"]

  let rec values : 'a . wrapped_value list -> 'a expr
    -> wrapped_value list =
    fun acc (type a) (expr: a t) ->
    match expr with
    | NULLABLE expr -> values acc expr
    | COMPARE (_, l, r) ->
      values (values acc l) r
    | SUB (l, r)
    | ADD (l, r) ->
      values (values acc l) r
    | OR (l, r)
    | AND (l, r) ->
      values (values acc l) r
    | IS_NOT_NULL expr ->
      values acc expr
    | FIELD _ -> acc
    | CONST (vl, ty) ->
      (MkWrapped (ty,vl)) :: acc
    | CONST_STATIC (_, _) -> acc
    | COERCETO (expr, _) -> values acc expr
    | AS (expr, _) -> values acc expr
    | REF _ -> acc
    | COUNT (_, exprs) -> values_expr_list acc exprs
    | COUNT_STAR -> acc
    | GROUP_CONCAT (_, l, None) ->
      values acc l
    | GROUP_CONCAT (_, l, Some r) ->
      values (values acc l) r
    | MAX (_, expr) -> values acc expr
    | MIN (_, expr) -> values acc expr
    | SUM (_, expr) -> values acc expr
    | TOTAL (_, expr) -> values acc expr
    | ABS expr -> values acc expr
    | CHANGES -> acc
    | GLOB (l, r) -> values (values acc l) r
    | COALESCE exprs ->
      List.fold_left ~init:acc ~f:values exprs
    | LIKE (l, r) -> values (values acc l) r
    | MIN_OF exprs ->
      List.fold_left ~init:acc ~f:values exprs
    | MAX_OF exprs ->
      List.fold_left ~init:acc ~f:values exprs
    | RANDOM -> acc
    | LOWER expr
    | UPPER expr -> values acc expr
  and values_expr_list :
    'a . wrapped_value list -> 'a expr_list -> wrapped_value list =
    fun acc (type a) (exprs: a expr_list) ->
    match exprs with
    | [] -> acc
    | h :: t -> values_expr_list (values acc h) t

  let rec pp  : 'a . Format.formatter -> 'a t -> unit =
    fun fmt (type a) (expr: a t) ->
    match expr with
    | NULLABLE expr -> pp fmt expr
    | COMPARE (op, l, r) ->
      let op = match op with
        | EQ -> "="
        | NEQ -> "!="
        | GT -> ">"
        | GE -> ">="
        | LT -> "<"
        | LE -> "<=" in
      Format.fprintf fmt "%a %s %a"
        pp l op pp r
    | ADD (l, r) ->
      Format.fprintf fmt "(%a) + (%a)"
        pp l pp r
    | SUB (l, r) ->
      Format.fprintf fmt "(%a) - (%a)"
        pp l pp r
    | AND (l,r) ->
      Format.fprintf fmt "%a AND %a"
        pp l pp r
    | OR (l, r) ->
      Format.fprintf fmt "%a AND %a"
        pp l pp r
    | IS_NOT_NULL expr -> 
      Format.fprintf fmt "%a IS NOT NULL"
        pp expr
    | FIELD (table_name, field) ->
      let table_name = resolve_table_name table_name in
      let field_name = Schema.field_name field in
      Format.fprintf fmt "%s.%s" table_name field_name
    | CONST_STATIC (vl, ty) -> begin match (ty: a ty), vl with
      | BOOL, true -> Format.fprintf fmt "TRUE"
      | BOOL, false -> Format.fprintf fmt "FALSE"
      | (INTEGER, i) -> Format.fprintf fmt "%d" i
      | (REAL, f) -> Format.fprintf fmt "%f" f
      | (TEXT, s) -> Format.fprintf fmt "'%s'" s
      | (BLOB, s) -> Format.fprintf fmt "'%s'" s

      | (NULLABLE_BOOL, None) -> Format.fprintf fmt "NULL"
      | (NULLABLE_BOOL, Some true) -> Format.fprintf fmt "TRUE"
      | (NULLABLE_BOOL, Some false) -> Format.fprintf fmt "FALSE"

      | (NULLABLE_INTEGER, None) -> Format.fprintf fmt "NULL"
      | (NULLABLE_INTEGER, Some i) -> Format.fprintf fmt "%d" i
      | (NULLABLE_REAL, None) -> Format.fprintf fmt "NULL"
      | (NULLABLE_REAL, Some f) -> Format.fprintf fmt "%f" f
      | (NULLABLE_TEXT, None) -> Format.fprintf fmt "NULL"
      | (NULLABLE_TEXT, Some s) -> Format.fprintf fmt "'%s'" s
      | (NULLABLE_BLOB, None) -> Format.fprintf fmt "NULL"
      | (NULLABLE_BLOB, Some s) -> Format.fprintf fmt "'%s'" s

    end
    | CONST (_, _) -> Format.fprintf fmt "?"
    | COERCETO (expr, _) -> pp fmt expr
    | AS (expr, name) ->
      Format.fprintf fmt "%a AS %s" pp expr name
    | REF (name,_) ->
      Format.fprintf fmt "%s" name
    | COUNT (false, exprs) ->
      Format.fprintf fmt "COUNT(%a)" pp_expr_list exprs
    | COUNT (true, exprs) -> 
      Format.fprintf fmt "COUNT(DISTINCT %a)" pp_expr_list exprs
    | COUNT_STAR ->
      Format.fprintf fmt "COUNT(*)"
    | MAX (false, expr) -> 
      Format.fprintf fmt "MAX(%a)" pp expr
    | MAX (true, expr) -> 
      Format.fprintf fmt "MAX(DISTINCT %a)" pp expr
    | MIN (false, expr) -> 
      Format.fprintf fmt "MIN(%a)" pp expr
    | MIN (true, expr) -> 
      Format.fprintf fmt "MIN(DISTINCT %a)" pp expr
    | SUM (false, expr) -> 
      Format.fprintf fmt "SUM(%a)" pp expr
    | SUM (true, expr) -> 
      Format.fprintf fmt "SUM(DISTINCT %a)" pp expr
    | TOTAL (false, expr) -> 
      Format.fprintf fmt "TOTAL(%a)" pp expr
    | TOTAL (true, expr) -> 
      Format.fprintf fmt "TOTAL(DISTINCT %a)" pp expr
    | GROUP_CONCAT (false, l, None) ->
      Format.fprintf fmt "GROUP_CONCAT(%a)" pp l
    | GROUP_CONCAT (false, l, Some r) ->
      Format.fprintf fmt "GROUP_CONCAT(%a, %a)" pp l pp r
    | GROUP_CONCAT (true, l, None) ->
      Format.fprintf fmt "GROUP_CONCAT(DISTINCT %a)" pp l
    | GROUP_CONCAT (true, l, Some r) ->
      Format.fprintf fmt "GROUP_CONCAT(DISTINCT %a, %a)" pp l pp r
    | ABS expr ->
      Format.fprintf fmt "ABS(%a)" pp expr
    | CHANGES -> Format.fprintf fmt "CHANGES()"
    | GLOB (pat, expr) ->
      Format.fprintf fmt "GLOB(%a, %a)" pp pat pp expr
    | COALESCE exprs ->
      Format.fprintf fmt "COALESCE(%a)"
        (Format.pp_print_list ~pp_sep:(fun fmt () ->
           Format.fprintf fmt ", ") pp) exprs
    | LIKE (pat, s) ->
      Format.fprintf fmt "LIKE(%a,%a)" pp pat pp s
    | MIN_OF exprs ->
      Format.fprintf fmt "MIN(%a)"
        (Format.pp_print_list ~pp_sep:(fun fmt () ->
           Format.fprintf fmt ", ") pp) exprs
    | MAX_OF exprs -> 
      Format.fprintf fmt "MAX(%a)"
        (Format.pp_print_list ~pp_sep:(fun fmt () ->
           Format.fprintf fmt ", ") pp) exprs
    | RANDOM ->
      Format.fprintf fmt "RANDOM()"
    | LOWER s ->
      Format.fprintf fmt "LOWER(%a)" pp s
    | UPPER s ->
      Format.fprintf fmt "UPPER(%a)" pp s
  and pp_expr_list_inner : 'a . Format.formatter -> 'a expr_list -> unit =
    fun fmt (type a) (ls: a expr_list) -> match ls with
      | [] -> ()
      | h :: t -> Format.fprintf fmt ", %a%a"
                    pp h pp_expr_list_inner t
  and pp_expr_list : 'a . Format.formatter -> 'a expr_list -> unit =
    fun fmt (type a) (ls: a expr_list) -> match ls with
      | [] -> ()
      | h :: t -> Format.fprintf fmt "%a%a"
                    pp h pp_expr_list_inner t

  let rec ty: 'a . 'a expr -> 'a ty = fun (type a) (expr: a expr) : a ty ->
    match expr with
    | SUB (_, _) -> INTEGER
    | ADD (_, _) -> INTEGER
    | AND (_, _) -> BOOL
    | OR (_, _) -> BOOL
    | COMPARE (_, _, _) -> BOOL
    | IS_NOT_NULL _ -> BOOL
    | FIELD (_, field) -> Schema.ty field
    | CONST (_, ty) -> ty
    | CONST_STATIC (_, ty) -> ty
    | COERCETO (_, ty) -> ty
    | AS (expr, _) -> ty expr
    | REF (_, ty) -> ty
    | COUNT (_, _) -> INTEGER
    | COUNT_STAR -> INTEGER
    | MAX (_, _) -> INTEGER
    | MIN (_, _) -> INTEGER
    | SUM (_, _) -> INTEGER
    | TOTAL (_, _) -> INTEGER
    | GROUP_CONCAT (_, _, _) -> TEXT
    | ABS _ -> INTEGER
    | CHANGES -> INTEGER
    | GLOB (_, _) -> BOOL
    | COALESCE [] -> failwith "empty coalesce not supported"
    | COALESCE (h::_) -> ty h
    | LIKE (_, _) -> BOOL
    | MIN_OF _ -> INTEGER
    | MAX_OF _ -> INTEGER
    | RANDOM -> INTEGER
    | LOWER _ -> TEXT
    | UPPER _ -> TEXT
    | NULLABLE expr -> Type.null_ty (ty expr)

  let rec ty_expr_list : 'a . 'a expr_list -> 'a ty_list =
    fun (type a) (ls: a expr_list) : a ty_list ->
    match ls with
    | [] -> Nil
    | h :: t ->
      Cons (ty h, ty_expr_list t)

  let i i = CONST (i,INTEGER)
  let f i = CONST (i,REAL)
  let s i = CONST (i,TEXT)
  let b i = CONST (i,BLOB)
  let bl i = CONST (i,BOOL)

  let i_stat i = CONST_STATIC (i,INTEGER)
  let f_stat i = CONST_STATIC (i,REAL)
  let s_stat i = CONST_STATIC (i,TEXT)
  let b_stat i = CONST_STATIC (i,BLOB)

  let nullable v = NULLABLE v

  let true_ = CONST_STATIC (true,BOOL)
  let false_ = CONST_STATIC (false,BOOL)

  let (+) l r = ADD (l, r)
  let (-) l r = SUB (l, r)

  let (=) l r = COMPARE (EQ, l, r)
  let (<>) l r = COMPARE (NEQ, l, r)
  let (<=) l r = COMPARE (LE, l, r)
  let (<) l r = COMPARE (LT, l, r)
  let (>) l r = COMPARE (GT, l, r)
  let (>=) l r = COMPARE (GE, l, r)

  let (&&) l r = AND (l, r)
  let (||) l r = OR (l, r)

  let (:=) l r = ASSIGN (l,r)

  let is_not_null expr = IS_NOT_NULL expr
  let coerce expr ty = COERCETO (expr,ty)
  let as_ expr ~name = AS (expr,name), REF (name, ty expr)

  let count ?(distinct=false) exprs = COUNT (distinct,exprs)
  let count_star = COUNT_STAR
  let max ?(distinct=false) expr = MAX (distinct,expr)
  let min ?(distinct=false) expr = MIN (distinct,expr)
  let sum ?(distinct=false) expr = SUM (distinct,expr)
  let total ?(distinct=false) expr = TOTAL (distinct,expr)
  let group_concat ?(distinct=false) ?sep_by l = GROUP_CONCAT (distinct, l, sep_by)


  let abs expr = ABS(expr)
  let changes = CHANGES
  let glob ~pat:x y = GLOB (x,y)
  let coalesce exprs = COALESCE exprs
  let like x ~pat:y = LIKE (x,y)

  let max_of exprs = MAX_OF exprs
  let min_of exprs = MIN_OF exprs

  let random = RANDOM
  let lower s = LOWER s
  let upper s = UPPER s

end

let pp_opt f fmt = function
  | None -> ()
  | Some vl -> Format.fprintf fmt "\n%a" f vl

let pp_ordering fmt = function
  | `ASC -> Format.fprintf fmt "ASC"
  | `DESC -> Format.fprintf fmt "DESC"

let pp_on_err : Format.formatter -> [ `ABORT | `FAIL | `IGNORE | `REPLACE | `ROLLBACK ] -> unit =
  fun fmt -> function
    | `IGNORE -> Format.fprintf fmt "OR IGNORE"
    | `REPLACE -> Format.fprintf fmt "OR REPLACE"
    | `ABORT -> Format.fprintf fmt "OR ABORT"
    | `FAIL -> Format.fprintf fmt "OR FAIL"
    | `ROLLBACK -> Format.fprintf fmt "OR ROLLBACK"

let rec query_values : 'a 'b. Expr.wrapped_value list -> ('a,'b) query -> Expr.wrapped_value list =
  fun acc (type a b) (query: (a,b) query) ->
  match query with
  | SELECT_CORE { exprs; table=_; join; where; group_by; having } ->
    let acc = Expr.values_expr_list acc exprs in
    let acc = List.fold_left ~init:acc ~f:(fun acc (MkJoin {table; on; join_op=_}) ->
      let acc = query_values acc table in
      let acc = Expr.values acc on in
      acc
    ) join in
    let acc = Option.map ~f:(Expr.values acc) where |> Option.value ~default:acc in
    let acc = Option.map ~f:(Expr.values_expr_list acc) group_by |> Option.value ~default:acc in
    let acc = Option.map ~f:(Expr.values acc) having |> Option.value ~default:acc in
    acc
  | SELECT { core; order_by; limit; offset } ->
    let acc = query_values acc core in
    let acc = Option.map ~f:(fun (_, expr) -> Expr.values acc expr) order_by
              |> Option.value ~default:acc in
    let acc = Option.map ~f:(Expr.values acc) limit |> Option.value ~default:acc in
    let acc = Option.map ~f:(Expr.values acc) offset |> Option.value ~default:acc in
    acc
  | DELETE { table=_; where } ->
    let acc = Option.map ~f:(Expr.values acc) where |> Option.value ~default:acc in
    acc
  | UPDATE { table=_; on_err=_; set; where } ->
    let acc = List.fold_left ~init:acc ~f:(fun acc (ASSIGN (vl, expr)) ->
      Expr.values (Expr.values acc vl) expr) set in
    let acc = Option.map ~f:(Expr.values acc) where |> Option.value ~default:acc in
    acc
  | INSERT { table=_; on_err=_; set } ->
    let acc = List.fold_left ~init:acc ~f:(fun acc (ASSIGN (vl, _)) ->
      Expr.values acc vl) set in
    let acc = List.fold_left ~init:acc ~f:(fun acc (ASSIGN (_, expr)) ->
      Expr.values acc expr) set in
    acc

let query_values query = List.rev (query_values [] query)

let rec pp_query: 'a 'b. Format.formatter ->
  ('a, 'b) query -> unit =
  fun fmt (type a b) (query: (a,b) query) ->
  (match query with
   | SELECT_CORE { exprs; table; join; where; group_by; having } ->
     Format.fprintf fmt
       "SELECT %a\nFROM %s%a%a%a%a"
       Expr.pp_expr_list exprs
       (resolve_table_name table)
       pp_join_list join
       (pp_opt (fun fmt vl ->
          Format.fprintf fmt "WHERE %a" Expr.pp vl))
       where
       (pp_opt (fun fmt vl ->
          Format.fprintf fmt "GROUP BY %a"
            Expr.pp_expr_list vl))
       group_by
       (pp_opt (fun fmt vl ->
          Format.fprintf fmt "HAVING %a"
            Expr.pp vl))
       having
   | SELECT { core; order_by; limit; offset } ->
     Format.fprintf fmt "%a%a%a%a"
       pp_query core
       (pp_opt (fun fmt (order,vl) ->
          Format.fprintf fmt "ORDER BY %a %a"
            Expr.pp vl pp_ordering order))
       order_by
       (pp_opt (fun fmt vl ->
          Format.fprintf fmt "LIMIT %a" Expr.pp vl))
       limit
       (pp_opt (fun fmt vl ->
          Format.fprintf fmt "OFFSET %a" Expr.pp vl))
       offset
   | DELETE { table; where } ->
     Format.fprintf fmt "DELETE FROM %s%a"
       (resolve_table_name table)
       (pp_opt (fun fmt vl ->
          Format.fprintf fmt "WHERE %a"
            Expr.pp vl
        ))
       where
   | UPDATE { table; on_err; set; where } ->
     Format.fprintf fmt "UPDATE%a %s\nSET %a%a"
       (pp_opt pp_on_err) on_err
       (resolve_table_name table)
       (Format.pp_print_list ~pp_sep:(fun fmt () ->
          Format.fprintf fmt ", ") pp_wrapped_assign) set
       (pp_opt (fun fmt vl -> Format.fprintf fmt "WHERE %a" Expr.pp vl))
       where
   | INSERT { table; on_err; set } ->
     let pp_field : 'a . Format.formatter -> 'a Expr.t -> unit =
       fun fmt (type a) (expr: a Expr.t) : unit ->
         match expr with
         | FIELD (_, field) -> Format.fprintf fmt "%s" (Schema.field_name field)
         | _ -> Format.kasprintf failwith "expected field for INSERT query, got %a" Expr.pp expr in
     Format.fprintf fmt "INSERT%a INTO %s (%a) VALUES (%a)"
       (pp_opt pp_on_err) on_err
       (resolve_table_name table)
       (Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
          (fun fmt (ASSIGN (fld, _)) -> Format.fprintf fmt "%a" pp_field fld))
       set
       (Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
          (fun fmt (ASSIGN (_, expr)) -> Format.fprintf fmt "%a" Expr.pp expr))
       set)
and pp_join : Format.formatter -> join -> unit =
  fun fmt (MkJoin { table; on; join_op }) ->
  Format.fprintf fmt "%a (%a) ON %a"
    pp_join_op join_op
    pp_query table
    Expr.pp on
and pp_join_list : Format.formatter -> join list -> unit =
  fun fmt ls ->
  match ls with
  | [] -> ()
  | h :: t ->
    Format.fprintf fmt " %a%a" pp_join h pp_join_list t
and pp_wrapped_assign: Format.formatter -> wrapped_assign -> unit =
  fun fmt (ASSIGN (vl, expr)) ->
  Format.fprintf fmt "%a = %a" Expr.pp vl Expr.pp expr

let show_query q = Format.asprintf "%a" pp_query q

let query_ret_ty: 'a 'b. ('a,'b) query -> 'a ty_list =
  fun (type a b) (query: (a,b) query) : a ty_list ->
  match query with
  | SELECT_CORE { exprs; table=_; join=_; where=_; group_by=_; having=_ } ->
    Expr.ty_expr_list exprs
  | SELECT { core=
               SELECT_CORE { exprs; table=_; join=_; where=_; group_by=_; having=_ };
             order_by=_; limit=_; offset=_ } ->
    Expr.ty_expr_list exprs      
  | DELETE _ -> Nil
  | UPDATE _ -> Nil
  | INSERT _ -> Nil

let select exprs ~from:table_name =
  SELECT_CORE {
    exprs; join=[]; table=table_name; where=None;
    group_by=None; having=None;
  }
let update ~table:table_name ~set =
  UPDATE { table=table_name; on_err=None; where=None; set; }
let insert ~table:table_name ~values:set =
  INSERT { table=table_name; on_err=None; set; }
let delete ~from:table_name =
  DELETE { table=table_name; where=None }

let filter : ('a,'c) filter_fun
  = fun  by (type a b) (table : (b, a) query) : (b, a) query ->
    let update_where where by =
      match where with
        None -> Some by
      | Some old_by -> Some Expr.(by && old_by) in
    match table with
    | SELECT_CORE { exprs; table; join; where; group_by; having } ->
      let where = update_where where by in
      SELECT_CORE { exprs; table; join; where; group_by; having }
    | SELECT { core=SELECT_CORE { exprs; table; join; where; group_by; having }; order_by; limit; offset } ->
      let where = update_where where by in
      SELECT { core=SELECT_CORE { exprs; table; join; where; group_by; having }; order_by; limit; offset }
    | DELETE { table; where } ->
      let where = update_where where by in
      DELETE { table; where }
    | UPDATE { table; on_err; set; where } ->
      let where = update_where where by in
      UPDATE { table; on_err; set; where } 
    | INSERT _ -> invalid_arg "filter on insert clause not supported"

let group_by : ('a,'b,'c) group_by_fun =
  fun by (type a b) (table : (b, a) query) : (b, a) query ->
  match table with
  | SELECT_CORE { exprs; table; join; where; group_by=_; having } ->
    SELECT_CORE { exprs; table; join; where; group_by=Some by; having }
  | SELECT { core=SELECT_CORE { exprs; table; join; where; group_by=_; having }; order_by; limit; offset } ->
    SELECT { core=SELECT_CORE { exprs; table; join; where; group_by=Some by; having }; order_by; limit; offset }
  | DELETE _ 
  | UPDATE _ 
  | INSERT _ -> invalid_arg "group by only supported on select clause"

let having : ('a,'c) having_fun =
  fun having (type a b) (table : (b, a) query) : (b, a) query ->
  match table with
  | SELECT_CORE { exprs; table; join; where; group_by; having=_ } ->
    SELECT_CORE { exprs; table; join; where; group_by; having=Some having }
  | SELECT { core=SELECT_CORE { exprs; table; join; where; group_by; having=_ }; order_by; limit; offset } ->
    SELECT { core=SELECT_CORE { exprs; table; join; where; group_by; having=Some having }; order_by; limit; offset }
  | DELETE _ 
  | UPDATE _ 
  | INSERT _ -> invalid_arg "group by only supported on select clause"

let join : ('a,'b,'d,'c) join_fun =
  fun ?(op=INNER) ~on (type a b c) (ot: (b, _) query)
    (table : (c, a) query)  ->
    match table with
    | SELECT_CORE { exprs; table; join; where; group_by; having } ->
      SELECT_CORE {
        exprs; table;
        join=join @ [MkJoin {
          table=ot;
          on;
          join_op=op
        }];
        where; group_by; having
      }
    | SELECT _
    | DELETE _ 
    | UPDATE _ 
    | INSERT _ ->
      invalid_arg "group by only supported on select clause"

let on_err : 'a . [`ABORT | `FAIL | `IGNORE | `REPLACE | `ROLLBACK ] -> ('c, 'a) query -> ('c, 'a) query =
  fun on_err (type a) (table : (_, a) query) : (_, a) query ->
  match table with
  | SELECT_CORE _
  | SELECT _
  | DELETE _ -> invalid_arg "on_err only supported for update and insert"
  | UPDATE { table; on_err=_; set; where } -> UPDATE { table; on_err=Some on_err; set; where }
  | INSERT { table; on_err=_; set } -> INSERT { table; on_err=Some on_err; set } 

let limit by (table: (_, [< `SELECT | `SELECT_CORE] as 'ty) query) : (_, [> `SELECT]) query =
  match table with
  | SELECT_CORE { exprs; table; join; where; group_by; having } ->
    SELECT { core=SELECT_CORE { exprs; table; join; where; group_by; having }; limit=Some by; offset=None; order_by=None}
  | SELECT { core; order_by; limit=_; offset } ->
    SELECT { core; order_by; limit=Some by; offset }

let offset by (table: (_, [< `SELECT | `SELECT_CORE] as 'ty) query) : (_, [> `SELECT]) query =
  match table with
  | SELECT_CORE { exprs; table; join; where; group_by; having } ->
    SELECT { core=SELECT_CORE { exprs; table; join; where; group_by; having }; limit=None; offset=Some by; order_by=None}
  | SELECT { core; order_by; limit; offset=_ } ->
    SELECT { core; order_by; limit; offset=Some by }

let order_by ?(direction=`ASC) field (table: (_, [< `SELECT | `SELECT_CORE] as 'ty) query) : (_, [> `SELECT]) query =
  match table with
  | SELECT_CORE { exprs; table; join; where; group_by; having } ->
    SELECT { core=SELECT_CORE { exprs; table; join; where; group_by; having }; limit=None; offset=None; order_by=Some (direction,field)}
  | SELECT { core; order_by=_; limit; offset } ->
    SELECT { core; order_by= Some(direction,field); limit; offset }

module Request = struct

  type ('res, !'multiplicity) caqti_request_inner =
      MkCaqti:'input ty_list * ('input, 'res, 'multiplicity) Caqti_request.t ->
        ('res, 'multiplicity) caqti_request_inner

  type ('res, !'multiplicity) t =
    ('res, 'multiplicity) caqti_request_inner * Expr.wrapped_value list

  type wrapped_ty_list = MkWrappedTyList : 'a ty_list -> wrapped_ty_list

  let rec extract_ty_list : Expr.wrapped_value list -> wrapped_ty_list =
    function
    | [] -> MkWrappedTyList Nil
    | Expr.MkWrapped (ty, _) :: rest ->
      let (MkWrappedTyList rest) = extract_ty_list rest in
      MkWrappedTyList (Cons (ty, rest))

  let rec unwrap : 'a . 'a ty_list * Expr.wrapped_value list -> 'a =
    fun (type a) ((tyls: a ty_list), (ls: Expr.wrapped_value list)) : a ->
    match tyls,ls with
    | Nil,[] -> ()
    | Cons (BOOL,  tyls), Expr.MkWrapped (BOOL, vl) :: ls  ->
      (vl, unwrap (tyls, ls))
    | Cons (INTEGER, tyls), Expr.MkWrapped (INTEGER, vl) :: ls  ->
      (vl, unwrap (tyls, ls))
    | Cons (REAL, tyls), Expr.MkWrapped (REAL, vl) :: ls  ->
      (vl, unwrap (tyls, ls))
    | Cons (TEXT, tyls), Expr.MkWrapped (TEXT, vl) :: ls  ->
      (vl, unwrap (tyls, ls))
    | Cons (BLOB, tyls), Expr.MkWrapped (BLOB, vl) :: ls  ->
      (vl, unwrap (tyls, ls))
    | Cons (ty, _), Expr.MkWrapped (oty, _) :: _  ->
      Format.ksprintf failwith "wrapped value list did not conform to specification - expected %s got %s"
        (Type.show ty) (Type.show oty)
    | Nil, _ | _, [] -> failwith "wrapped value list length mismatch"

  module QueryMap = Type.Map (struct type ('a,'b) t = ('a,'b) caqti_request_inner end)

  let cache_zero : (string, [ `Zero ] QueryMap.t) Base.Hashtbl.t = Hashtbl.Poly.create ()
  let cache_one : (string, [ `One ] QueryMap.t) Hashtbl.t = Hashtbl.Poly.create ()
  let cache_zero_or_one : (string, [ `Zero | `One ] QueryMap.t) Base.Hashtbl.t = Hashtbl.Poly.create ()
  let cache_many : (string, [ `Many | `Zero | `One ] QueryMap.t) Base.Hashtbl.t = Hashtbl.Poly.create ()

  let make_zero : 'b . (unit,'b) query -> (unit, [`Zero]) t =
    fun (type b) (query: (unit,b) query) : (unit, [`Zero]) t ->
    let query_repr = Caqti_query.of_string_exn (Format.asprintf "%a" pp_query query) in
    let query_values = query_values query in
    (* Format.printf "DEBUG: query is: %a, values are [%a]@.%!" Caqti_query.pp query_repr
     *   (Format.pp_print_list Expr.pp_wrapped_value) query_values; *)
    let (MkWrappedTyList query_value_ty) = extract_ty_list query_values in
    let request = Caqti_request.create (Type.ty_list_to_caqti_ty query_value_ty) Caqti_type.unit Caqti_mult.zero
                    (fun _ -> query_repr) in
    MkCaqti (query_value_ty,request), query_values

  let make_zero : 'b . (unit,'b) query -> (unit, [`Zero]) t =
    fun (type b) (query: (unit,b) query) : (unit, [`Zero]) t ->
    let query_txt = Format.asprintf "%a" pp_query query in
    let query_ty = query_ret_ty query in
    let query_values = query_values query in
    let ty_map = match Hashtbl.find cache_zero query_txt with
      | Some ty_map -> ty_map
      | None -> QueryMap.empty in
    match QueryMap.lookup_opt ty_map ~key:query_ty with
    | Some res -> res, query_values
    | None ->
      let (query_inner, _) as query = make_zero query in
      let ty_map = QueryMap.insert ty_map ~key:query_ty ~data:query_inner in
      Hashtbl.set cache_zero ~key:query_txt ~data:ty_map;
      query

  let make_one : 'a 'b . ('a,'b) query -> ('a, [`One]) t =
    fun (type a b) (query: (a,b) query) : (a, [`One]) t ->
    let query_repr = Caqti_query.of_string_exn (Format.asprintf "%a" pp_query query) in
    let query_values = query_values query in
    let (MkWrappedTyList query_value_ty) = extract_ty_list query_values in
    let ret_ty = query_ret_ty query in
    let request = Caqti_request.create (Type.ty_list_to_caqti_ty query_value_ty) (Type.ty_list_to_caqti_ty ret_ty)
                    Caqti_mult.one (fun _ -> query_repr) in
    MkCaqti (query_value_ty,request), query_values

  let make_one : 'a 'b . ('a,'b) query -> ('a, [`One]) t =
    fun (type a b) (query: (a,b) query) : (a, [`One]) t ->
    let query_txt = Format.asprintf "%a" pp_query query in
    let query_ty = query_ret_ty query in
    let query_values = query_values query in
    let ty_map = match Hashtbl.find cache_one query_txt with
      | Some ty_map -> ty_map
      | None -> QueryMap.empty in
    match QueryMap.lookup_opt ty_map ~key:query_ty with
    | Some res -> res, query_values
    | None ->
      let (query_values, _) as query = make_one query in
      let ty_map = QueryMap.insert ty_map ~key:query_ty ~data:query_values in
      Hashtbl.set cache_one ~key:query_txt ~data:ty_map;
      query

  let make_zero_or_one : 'a 'b . ('a,'b) query -> ('a, [`Zero | `One]) t =
    fun (type a b) (query: (a,b) query) : (a, [`Zero | `One]) t ->
    let query_repr = Caqti_query.of_string_exn (Format.asprintf "%a" pp_query query) in
    let query_values = query_values query in
    let (MkWrappedTyList query_value_ty) = extract_ty_list query_values in
    let ret_ty = query_ret_ty query in
    let request = Caqti_request.create (Type.ty_list_to_caqti_ty query_value_ty) (Type.ty_list_to_caqti_ty ret_ty)
                    Caqti_mult.zero_or_one (fun _ -> query_repr) in
    MkCaqti (query_value_ty,request), query_values

  let make_zero_or_one : 'a 'b . ('a,'b) query -> ('a, [`Zero | `One]) t =
    fun (type a b) (query: (a,b) query) : (a, [`Zero | `One]) t ->
    let query_txt = Format.asprintf "%a" pp_query query in
    let query_ty = query_ret_ty query in
    let query_values = query_values query in
    let ty_map = match Hashtbl.find cache_zero_or_one query_txt with
      | Some ty_map -> ty_map
      | None -> QueryMap.empty in
    match QueryMap.lookup_opt ty_map ~key:query_ty with
    | Some res -> res, query_values
    | None ->
      let (query_inner, _) as query = make_zero_or_one query in
      let ty_map = QueryMap.insert ty_map ~key:query_ty ~data:query_inner in
      Hashtbl.set cache_zero_or_one ~key:query_txt ~data:ty_map;
      query

  let make_many : 'a 'b . ('a,'b) query -> ('a, [`Many | `Zero | `One]) t =
    fun (type a b) (query: (a,b) query) : (a, [`Many | `Zero | `One]) t ->
    let query_repr = Caqti_query.of_string_exn (Format.asprintf "%a" pp_query query) in
    let query_values = query_values query in
    let (MkWrappedTyList query_value_ty) = extract_ty_list query_values in
    let ret_ty = query_ret_ty query in
    let request = Caqti_request.create (Type.ty_list_to_caqti_ty query_value_ty) (Type.ty_list_to_caqti_ty ret_ty)
                    Caqti_mult.zero_or_more (fun _ -> query_repr) in
    MkCaqti (query_value_ty,request), query_values

  let make_many : 'a 'b . ('a,'b) query -> ('a, [`Many | `Zero | `One]) t =
    fun (type a b) (query: (a,b) query) : (a, [`Many | `Zero | `One]) t ->
    let query_txt = Format.asprintf "%a" pp_query query in
    let query_ty = query_ret_ty query in
    let query_values = query_values query in
    let ty_map = match Hashtbl.find cache_many query_txt with
      | Some ty_map -> ty_map
      | None -> QueryMap.empty in
    match QueryMap.lookup_opt ty_map ~key:query_ty with
    | Some res -> res, query_values
    | None ->
      let (query_inner, _) as query = make_many query in
      let ty_map = QueryMap.insert ty_map ~key:query_ty ~data:query_inner in
      Hashtbl.set cache_many ~key:query_txt ~data:ty_map;
      query

end

let exec : (module Caqti_lwt.CONNECTION) ->
  (unit,[< `Zero ]) Request.t -> (unit, [> Caqti_error.call_or_retrieve ]) result Lwt.t =
  fun (module DB: Caqti_lwt.CONNECTION) ((MkCaqti (inps,req),wrapp_value): (unit,_) Request.t) ->
  let data = Request.unwrap (inps,wrapp_value) in
  DB.exec req data

let find : 'a . (module Caqti_lwt.CONNECTION) ->
  ('a,[< `One ]) Request.t -> ('a, [> Caqti_error.call_or_retrieve ]) result Lwt.t =
  fun (module DB: Caqti_lwt.CONNECTION) (type a) ((MkCaqti (inps,req),wrapp_value): (a,_) Request.t) ->
  let data = Request.unwrap (inps,wrapp_value) in
  DB.find req data

let find_opt : 'a . (module Caqti_lwt.CONNECTION) ->
  ('a,[< `One | `Zero ]) Request.t -> ('a option, [> Caqti_error.call_or_retrieve ]) result Lwt.t =
  fun (module DB: Caqti_lwt.CONNECTION) (type a) ((MkCaqti (inps,req),wrapp_value): (a,_) Request.t) ->
  let data = Request.unwrap (inps,wrapp_value) in
  DB.find_opt req data

let collect_list : 'a . (module Caqti_lwt.CONNECTION) ->
  ('a,[< `Many | `One | `Zero ]) Request.t -> ('a list, [> Caqti_error.call_or_retrieve ]) result Lwt.t =
  fun (module DB: Caqti_lwt.CONNECTION) (type a) ((MkCaqti (inps,req),wrapp_value): (a,_) Request.t) ->
  let data = Request.unwrap (inps,wrapp_value) in
  DB.collect_list req data
