type table_name

type 'a ty

module Type : sig
  type 'a t = 'a ty
  val bool : bool t
  val int : int t
  val real : float t
  val text : string t
  val blob : string t
end

type join_op = LEFT | RIGHT | INNER

type 'a expr

type 'a expr_list =
  [] : unit expr_list
| (::) : ('a expr * 'b expr_list) -> ('a * 'b) expr_list

type wrapped_assign

module Schema : sig

  type conflict_clause =
    [ `ABORT | `FAIL | `IGNORE | `REPLACE | `ROLLBACK ]
  type foreign_conflict_clause =
    [ `CASCADE | `NO_ACTION | `RESTRICT | `SET_DEFAULT | `SET_NULL ]

  type 'a constraint_

  type 'a field

  val field : ?constraints:[`Column] constraint_ list -> string -> ty:'a ty -> 'a field

  type 'a table =
    [] : unit table
  | (::) : ('a field * 'b table) -> ('a * 'b) table

  val primary_key :
    ?name:string ->
    ?ordering:[ `ASC | `DESC ] ->
    ?on_conflict:conflict_clause ->
    ?auto_increment:bool -> unit -> [ `Column ] constraint_

  val table_primary_key :
    ?name:string ->
    ?on_conflict:conflict_clause -> string list -> [ `Table ] constraint_

  val not_null :
    ?name:string ->
    ?on_conflict:conflict_clause -> unit -> [ `Column ] constraint_

  val unique :
    ?name:string ->
    ?on_conflict:conflict_clause -> unit -> [ `Column ] constraint_

  val table_unique :
    ?name:string ->
    ?on_conflict:conflict_clause -> string list -> [ `Table ] constraint_

  val foreign_key :
    ?name:string ->
    ?on_update:foreign_conflict_clause ->
    ?on_delete:foreign_conflict_clause ->
    table:table_name ->
    columns:'a expr_list -> unit -> [ `Column ] constraint_

  val table_foreign_key :
    ?name:string ->
    ?on_update:foreign_conflict_clause ->
    ?on_delete:foreign_conflict_clause ->
    table:table_name ->
    columns:'a expr_list -> string list -> [ `Table ] constraint_

end

type (_, 'res) query
val pp_query: Format.formatter -> ('out,'ty) query -> unit
val show_query: ('out,'ty) query -> string

type ('a, 'c) filter_fun = bool expr -> ('c, 'a) query -> ('c, 'a) query
  constraint 'a = [< `DELETE | `SELECT | `SELECT_CORE | `UPDATE ]

type ('a, 'b, 'c) group_by_fun =
  'b expr_list -> ('c, 'a) query -> ('c, 'a) query
  constraint 'a = [< `SELECT | `SELECT_CORE ]

type ('a, 'c) having_fun = bool expr -> ('c, 'a) query -> ('c, 'a) query
  constraint 'a = [< `SELECT | `SELECT_CORE ]

type ('a, 'b, 'd, 'c) join_fun =
  ?op:join_op ->
  on:bool expr -> ('b, 'd) query -> ('c, 'a) query -> ('c, 'a) query
  constraint 'a = [< `SELECT_CORE ] constraint 'd = [< `SELECT_CORE | `SELECT ]

type ('a, 'b, 'c) on_err_fun = 'b -> ('c, 'a) query -> ('c, 'a) query
  constraint 'a = [> `INSERT | `UPDATE ]
  constraint 'b = [< `ABORT | `FAIL | `IGNORE | `REPLACE | `ROLLBACK ]

val declare_table :
  ?constraints:[`Table] Schema.constraint_ list -> name:string -> 'a Schema.table -> table_name * 'a expr_list

val initialise : (module Caqti_lwt.CONNECTION) -> (unit, [> `Msg of string ]) Lwt_result.t

module Expr : sig
  type 'a t = 'a expr
  val pp: Format.formatter -> 'a t -> unit

  val i : int -> int expr
  val f : float -> float expr
  val s : string -> string expr
  val b : string -> string expr
  val bl : bool -> bool expr

  val i_stat : int -> int expr

  val f_stat : float -> float expr

  val s_stat : string -> string expr

  val b_stat : string -> string expr

  val nullable: 'a t -> 'a option t
  
  val true_ : bool expr

  val false_ : bool expr

  val ( + ) : int expr -> int expr -> int expr
  val ( - ) : int expr -> int expr -> int expr

  val ( = ) : 'a expr -> 'a expr -> bool expr
  val ( <> ) : 'a expr -> 'a expr -> bool expr
  val ( <= ) : 'a expr -> 'a expr -> bool expr
  val ( < ) : 'a expr -> 'a expr -> bool expr
  val ( > ) : 'a expr -> 'a expr -> bool expr
  val ( >= ) : 'a expr -> 'a expr -> bool expr
  val ( && ) : bool expr -> bool expr -> bool expr
  val ( || ) : bool expr -> bool expr -> bool expr
  val ( := ) : 'a expr -> 'a expr -> wrapped_assign

  val is_not_null : 'a expr -> bool expr

  val coerce : 'a expr -> 'b ty -> 'b expr

  val as_ : 'a expr -> name:string -> 'a expr * 'a expr

  val count : ?distinct:bool -> 'a expr_list -> int expr

  val count_star : int expr

  val max : ?distinct:bool -> int expr -> int expr

  val min : ?distinct:bool -> int expr -> int expr

  val sum : ?distinct:bool -> int expr -> int expr

  val total : ?distinct:bool -> int expr -> int expr

  val group_concat: ?distinct:bool -> ?sep_by:string t -> string t -> string t

  val abs : int expr -> int expr

  val changes : int expr

  val glob : pat:string expr -> string expr -> bool expr

  val coalesce : 'a expr list -> 'a expr

  val like : string expr -> pat:string expr -> bool expr

  val max_of : int expr list -> int expr

  val min_of : int expr list -> int expr

  val random : int expr

  val lower : string expr -> string expr

  val upper : string expr -> string expr

end

val select :
  'a expr_list -> from:table_name -> ('a, [> `SELECT_CORE ]) query

val update :
  table:table_name -> set:wrapped_assign list -> (unit, [> `UPDATE ]) query

val insert :
  table:table_name ->
  values:wrapped_assign list -> (unit, [> `INSERT ]) query

val delete : from:table_name -> (unit, [> `DELETE ]) query

val filter :
  ([< `DELETE | `SELECT | `SELECT_CORE | `UPDATE ], 'c) filter_fun

val group_by : ([< `SELECT | `SELECT_CORE ], 'b, 'c) group_by_fun

val having : ([< `SELECT | `SELECT_CORE ], 'c) having_fun

val join : ([ `SELECT_CORE ], 'b, [< `SELECT_CORE | `SELECT ], 'c) join_fun

val on_err : [ `ABORT | `FAIL | `IGNORE | `REPLACE | `ROLLBACK ] -> (unit, 'a) query -> (unit, 'a) query

val limit : int expr -> ('a, [ `SELECT | `SELECT_CORE ]) query -> ('a, [> `SELECT ]) query

val offset : int expr -> ('a, [ `SELECT | `SELECT_CORE ]) query -> ('a, [> `SELECT ]) query

val order_by : ?direction:[ `ASC | `DESC ] -> 'a expr -> ('b, [ `SELECT | `SELECT_CORE ]) query -> ('b, [> `SELECT ]) query

module Request : sig
  type ('res, 'multiplicity) t

  val make_zero : (unit, 'b) query -> (unit, [ `Zero ]) t

  val make_one : ('a, 'b) query -> ('a, [ `One ]) t

  val make_zero_or_one : ('a, 'b) query -> ('a, [ `One | `Zero ]) t

  val make_many : ('a, 'b) query -> ('a, [ `Many | `One | `Zero ]) t

end

val exec : (module Caqti_lwt.CONNECTION) -> (unit, [< `Zero ]) Request.t ->
  (unit, [> Caqti_error.call_or_retrieve ]) result Lwt.t

val find : (module Caqti_lwt.CONNECTION) -> ('a, [< `One ]) Request.t ->
  ('a, [> Caqti_error.call_or_retrieve ]) result Lwt.t

val find_opt : (module Caqti_lwt.CONNECTION) ->
  ('a, [< `One | `Zero ]) Request.t ->
  ('a option, [> Caqti_error.call_or_retrieve ]) result Lwt.t

val collect_list :
  (module Caqti_lwt.CONNECTION) ->
  ('a, [< `Many | `One | `Zero ]) Request.t ->
  ('a list, [> Caqti_error.call_or_retrieve ]) result Lwt.t
