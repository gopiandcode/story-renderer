let run debug port =
  Story_renderer.Server.run ~debug:debug port

open Cmdliner

let port =
  let info =
    Arg.info ~doc:"Port to run server on, defaults to 8080." ["p"; "port"] in
  Arg.value @@ Arg.opt Arg.(some int) None info

let debug =
  let info =
    Arg.info ~doc:"Whether to run in debug mode." ["D"; "debug"]
  in
  Arg.value @@ Arg.flag info


let () =
  exit @@ Cmd.eval @@ Cmd.v Cmd.(info "story-renderer") Term.(const run $ debug $ port)
