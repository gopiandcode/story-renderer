open! Core

module StringMultiSet = Utils.StringMultiSet
module StringSet = Set.Make (String)
module StringMap = Map.Make (String)
module IntMap = Map.Make (Int)
module Data = Story_renderer_data

let is_valid_clause (App (fn, args) : Data.clause) =
  not (String.is_empty fn) &&
  not (List.exists ~f:String.is_empty args) &&
  String.length fn > 0 && Char.is_lowercase (String.get fn 0)

type color =
  | Blue | Green | Yellow | Orange | Red
[@@deriving sexp, equal]

let random_color () =
  match Random.int 5 with
  | 0 -> Blue
  | 1 -> Green
  | 2 -> Yellow
  | 3 -> Orange
  | _ -> Red

let to_color_class = function
  | Blue -> "timeline-blue"
  | Green -> "timeline-green"
  | Yellow -> "timeline-yellow"
  | Orange -> "timeline-orange"
  | Red -> "timeline-red"

let is_variable var =
  String.length var > 0 && Char.is_uppercase (String.get var 0)

module Rule = struct

  type t = {
    variables: StringMultiSet.t;
    head: Data.clause;
    properties: Data.clause list;
    preconditions: (int * bool * Data.clause) list;
    initiated_fluents: Data.clause list;
    terminated_fluents: Data.clause list;
  }
  [@@deriving sexp, equal, fields]

  let of_raw
      ({head;constraints;preconditions;initiated_fluents;terminated_fluents}:
         Data.rule) =
    let variables = StringMultiSet.empty in
    {
      variables;
      head;
      properties=constraints;
      preconditions=List.map
          ~f:(fun ({offset;negated;clause} : Data.temporal_property) ->
              (offset,negated,clause)
            ) preconditions;
      initiated_fluents;
      terminated_fluents
    }


  let to_raw {
      variables=_;
      head;
      properties;
      preconditions;
      initiated_fluents;
      terminated_fluents
    } =
    let preconditions = List.filter_map ~f:(fun (offset,negated,clause) ->
        if is_valid_clause clause
        then Some ({offset;negated;clause}:Data.temporal_property)
        else None
      ) preconditions in
    if is_valid_clause head && not (List.is_empty preconditions)
    then
      Some ({ head; constraints=List.filter ~f:is_valid_clause properties;
         preconditions;
         initiated_fluents=List.filter ~f:is_valid_clause initiated_fluents;
         terminated_fluents=List.filter ~f:is_valid_clause terminated_fluents;
       }: Data.rule)
    else
      None

  let empty = {
    variables=StringMultiSet.of_set (StringSet.singleton "T");
    head=App ("",[]);
    properties=[];
    preconditions=[];
    initiated_fluents=[];
    terminated_fluents=[]
  }

  let normalize_clause (Data.App (fn,args)) = Data.App (String.lowercase fn, args)

  let update_variables ?new_clause ?old_clause model =
    let variables =
      match old_clause with
      | Some (Data.App (_, args)) ->
        List.fold_left ~init:model.variables ~f:(fun variables var ->
            if is_variable var
            then StringMultiSet.remove variables var
            else variables
          ) args
      | _ ->  model.variables in
    let variables =
      match new_clause with
      | Some (Data.App (_, args)) ->
        List.fold_left ~init:variables ~f:(fun variables var ->
            if is_variable var
            then StringMultiSet.add variables var
            else variables
          ) args
      | _ ->  model.variables in
    {model with variables}

  let set_head_clause clause model =
    let clause = normalize_clause clause in
    let model = update_variables ~old_clause:model.head ~new_clause:clause model in
    {model with head=clause}

  let add_property clause model =
    let clause = normalize_clause clause in
    let model = update_variables ~new_clause:clause model in
    {model with properties=model.properties @ [clause]}

  let update_nth_property ~index clause model =
    let clause = normalize_clause clause in
    let old_clause = ref None in
    let properties =
      Utils.update_nth_with index model.properties (fun old_clause' ->
          old_clause := Some old_clause';
          clause
        ) in
    let model = update_variables ?old_clause:!old_clause ~new_clause:clause model in
    !old_clause, {model with properties}

  let remove_nth_property ~index model =
    let old_clause, properties =
      Utils.take_nth index model.properties in
    let model = update_variables ?old_clause model in
    old_clause, {model with properties}

  let add_initiated_fluent clause model =
    let clause = normalize_clause clause in
    let model = update_variables ~new_clause:clause model in
    {model with initiated_fluents= model.initiated_fluents @ [clause]}

  let update_nth_initiated_fluent ~index clause model =
    let clause = normalize_clause clause in
    let old_clause = ref None in
    let initiated_fluents = Utils.update_nth_with index model.initiated_fluents (fun old_clause' ->
        old_clause := Some old_clause';
        clause
      ) in
    let model = update_variables ?old_clause:!old_clause ~new_clause:clause model in
    !old_clause, {model with initiated_fluents}

  let remove_nth_initiated_fluent ~index model =
    let old_clause, initiated_fluents = Utils.take_nth index model.initiated_fluents in
    let model = update_variables ?old_clause model in
    old_clause, {model with initiated_fluents}

  let add_terminated_fluent clause model =
    let clause = normalize_clause clause in
    let model = update_variables ~new_clause:clause model in
    {model with terminated_fluents= model.terminated_fluents @ [clause]}

  let update_nth_terminated_fluent ~index clause model =
    let clause = normalize_clause clause in
    let old_clause = ref None in
    let terminated_fluents = Utils.update_nth_with index model.terminated_fluents (fun old_clause' ->
        old_clause := Some old_clause';
        clause
      ) in
    let model = update_variables ?old_clause:!old_clause ~new_clause:clause model in
    !old_clause, {model with terminated_fluents}

  let remove_nth_terminated_fluent ~index model =
    let old_clause, terminated_fluents = Utils.take_nth index model.terminated_fluents in
    let model = update_variables ?old_clause model in
    old_clause, {model with terminated_fluents}

  let add_precondition (time,negated,clause) model =
    let clause = normalize_clause clause in
    let model = update_variables ~new_clause:clause model in
    {model with preconditions=model.preconditions @ [(time, negated, clause)]}

  let update_nth_precondition_value ~index clause model =
    let clause = normalize_clause clause in
    let old_clause = ref None in
    let preconditions = Utils.update_nth_with index model.preconditions
        (fun (time, negated, old_clause') ->
           old_clause := Some old_clause';
           (time, negated, clause)
        ) in
    let model = update_variables ?old_clause:!old_clause ~new_clause:clause model in
    !old_clause, {model with preconditions}

  let update_nth_precondition_time ~index time model =
    let preconditions = Utils.update_nth_with index model.preconditions
        (fun (_, negated, clause) ->
           (time, negated, clause)
        ) in
    {model with preconditions}

  let update_nth_precondition_negated ~index negated model =
    let preconditions = Utils.update_nth_with index model.preconditions
        (fun (time, _, clause) ->
           (time, negated, clause)
        ) in
    {model with preconditions}

  let remove_nth_precondition ~index model =
    let old_clause, preconditions = Utils.take_nth index model.preconditions in
    let model =
      update_variables ?old_clause:(Option.map ~f:(fun (_,_,cl) -> cl)
                                      old_clause) model in
    old_clause, {model with preconditions}

end


module State = struct

  type event =
      Region of { start: int option; stop: int option; clause: Data.clause }
    | Event of { time: int; clause: Data.clause }
  [@@deriving sexp, equal]

  type timeline_row = {
    header: string;
    events: (color * event) list;
  }
  [@@deriving sexp, equal, fields]

  let event_overlaps
        ({ start=s1; end_=e1; _ }: Data.event)
        ({ start=s2; end_=e2; _ }: Data.event) =
    let e1_before_s2 =
      match e1, s2 with
      | Some e1, Some s2 -> Some (e1 <= s2)
      | _ -> None in
    let e2_before_s1 =
      match e2, s1 with
      | Some e2, Some s1 -> Some (e2 <= s1)
      | _ -> None in
    let doesnt_overlap =
      match e1_before_s2, e2_before_s1 with
      | Some true, _ | _, Some true -> true
      | _ -> false in
    not doesnt_overlap

  let raw_event_to_event ({ start; end_; clause }: Data.event) : event =
    Region { start; stop=end_; clause }

  let sort_timeline timeline =
    let event_start = function Region {start;_} -> start | Event {time;_} -> Some time in
    List.sort timeline ~compare:(fun (_, le) (_, lr) -> Option.compare Int.compare (event_start le) (event_start lr))

  let interval_header clause =
    let Data.App (fn, args) = clause in
    Format.asprintf "%s(%a)" fn
      (Format.pp_print_list
         ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
         Format.pp_print_string)
      (List.mapi args ~f:(fun ind arg ->
         if String.is_empty arg || String.length arg = 0
         then Char.to_string (Char.of_int_exn (Char.to_int 'a' + ind))
         else Char.to_string (Char.uppercase (String.get arg 0))
       ))

  let generic_intervals_to_timelines : Data.event list -> timeline_row list =
    let rec loop color acc_ls events timeline ls =
      match ls with
      | [] -> timeline, acc_ls
      | event :: rest ->
        if List.exists ~f:(event_overlaps event) events
        then loop color (event :: acc_ls) events timeline rest
        else
          let timeline = (color, raw_event_to_event event) :: timeline in
          loop color acc_ls (event :: events) timeline rest in
    let rec until_fixpoint header timelines events =
      let color = random_color () in
      let timeline, events = loop color [] [] [] events in
      let timeline = sort_timeline timeline in
      let timelines = {header;events=timeline} :: timelines in
      match events with
      | [] -> timelines
      | _ :: _ -> until_fixpoint header timelines events in
    fun events ->
      match events with
      | [] -> []
      | ({ clause; _ } :: _) ->
        let header = interval_header clause in
        until_fixpoint header [] events

  let intervals_to_timelines : Data.event list -> timeline_row list =
    fun evs ->
    let at_evs, evs =
      List.partition_tf evs ~f:(function {clause=App ("at", [_;_]);_} -> true | _ -> false) in
    let place_map = Hashtbl.create (module String) in
    let at_evs =
      Sequence.of_list at_evs
      |> Sequence.map ~f:(fun ({clause=App (_, [person; place]);_} as ev: Data.event) -> (person, place, ev))[@warning "-8"]
      |> Sequence.fold ~init:StringMap.empty ~f:(fun acc (person, place, ev) ->
        let color = Hashtbl.find_or_add place_map place ~default:random_color in
        StringMap.update acc person ~f:(fun o -> (color, raw_event_to_event ev) :: Option.value ~default:[] o)
      )
      |> (fun s -> StringMap.to_sequence s)
      |> Sequence.map ~f:(fun (name, evs) ->
        let events = sort_timeline evs in
        let header = Format.sprintf "at(%s,L)" name in
        {header;events}
      ) |> Sequence.to_list in
    let rest_evs = generic_intervals_to_timelines evs in
    at_evs @ rest_evs

  let events_to_timelines :
    (int * Data.clause) list -> timeline_row list =
    let header clause =
      let Data.App (fn, args) = clause in
      Format.asprintf "%s(%a)" fn
        (Format.pp_print_list
           ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
           Format.pp_print_string)
        (List.mapi args ~f:(fun ind arg ->
           if String.is_empty arg || String.length arg = 0
           then Char.to_string (Char.of_int_exn (Char.to_int 'a' + ind))
           else Char.to_string (Char.uppercase (String.get arg 0))
         )) in
    let event_overlaps (s1, _) (s2, _) = Int.equal s1 s2 in
    let raw_event_to_event (time, clause) =
      Event {time; clause} in
    let rec loop color acc_ls events timeline ls =
      match ls with
      | [] -> timeline, acc_ls
      | event :: rest ->
        if List.exists ~f:(event_overlaps event) events
        then loop color (event :: acc_ls) events timeline rest
        else
          let timeline = (color, raw_event_to_event event) :: timeline in
          loop color acc_ls (event :: events) timeline rest in
    let rec until_fixpoint header timelines events =
      let color = random_color () in
      let timeline, events = loop color [] [] [] events in
      let timelines = {header;events=timeline} :: timelines in
      match events with
      | [] -> timelines
      | _ :: _ -> until_fixpoint header timelines events in
    fun events ->
      match events with
      | [] -> []
      | (_, clause) :: _ ->
        let header = header clause in
        until_fixpoint header [] events

  let of_model
        ({ intervals; events }: Data.model) : timeline_row list =
    let interval_list = 
      List.fold_left ~init:StringMap.empty
        ~f:(fun acc (
          { clause=Data.App (fn, _); _ } as ev: Data.event
        ) ->
          StringMap.update acc fn ~f:(fun entry ->
            let entry = Option.value ~default:[] entry in
            ev :: entry
          )
        ) intervals in
    let events_list = 
      List.fold_left ~init:StringMap.empty
        ~f:(fun acc ((_, Data.App (fn, _))  as ev) ->
          StringMap.update acc fn ~f:(fun entry ->
            let entry = Option.value ~default:[] entry in
            ev :: entry
          )
        ) events in

    let interval_timelines =
      StringMap.to_sequence interval_list
      |> Sequence.concat_map ~f:(fun (_, ls) ->
        intervals_to_timelines ls
        |> Sequence.of_list
      ) in
    let event_timelines =
      StringMap.to_sequence events_list
      |> Sequence.concat_map ~f:(fun (_, ls) ->
        events_to_timelines ls
        |> Sequence.of_list
      ) in
    let timelines =
      Sequence.append interval_timelines event_timelines in
    Sequence.to_list timelines

  type t = {
    rules: Rule.t list;
    fixed_clauses: (int * bool * Data.clause) list;
    properties: Data.clause list;
    known_literals: StringMultiSet.t;
    known_relations: StringMultiSet.t;
    max_time: int;

    model: timeline_row list option;

    errors: string list;
  }
  [@@deriving sexp, equal, fields]

  let of_raw model ({
    rules;
    scenario={name=_;temporal_clauses; constant_clauses};
    max_time
  } : Data.problem_spec) : t =
    let known_literals = StringMultiSet.empty in
    let known_relations = StringMultiSet.empty in
    {
      rules=List.map ~f:Rule.of_raw rules;
      max_time;
      fixed_clauses=temporal_clauses;
      properties=constant_clauses;
      model=model.model;
      errors=model.errors;
      known_literals;
      known_relations;
    }

  let to_raw {
    rules;fixed_clauses;
    properties; max_time; _
  } : Data.problem_spec =
    {
      rules=List.filter_map ~f:Rule.to_raw rules;
      max_time;
      scenario={
        name="default";
        temporal_clauses=List.filter ~f:(fun (_,_,cl) -> is_valid_clause cl) fixed_clauses;
        constant_clauses=List.filter ~f:is_valid_clause properties
      }
    }

  let empty = {
    rules=[];
    max_time=20;
    known_literals=StringMultiSet.empty;
    known_relations=StringMultiSet.empty;
    fixed_clauses=[];
    properties=[];
    errors=[];
    model=None
  }

  let normalize_clause (Data.App (fn, args)) =
    Data.App (String.lowercase fn, List.map ~f:String.lowercase args)

  let update_cached
        ?old_clause
        ?new_clause model =
    let known_literals =
      match old_clause with
      | Some (Data.App (_, old_args)) ->
        List.fold_left
          ~f:StringMultiSet.remove
          ~init:model.known_literals
          (List.filter ~f:(Fun.negate is_variable)
             old_args)
      | _ -> model.known_literals in
    let known_literals =
      match new_clause with
      | Some (Data.App (_, new_args)) ->
        List.fold_left
          ~f:StringMultiSet.add
          ~init:known_literals
          (List.filter ~f:(Fun.negate is_variable)
             (List.filter ~f:(Fun.negate String.is_empty) new_args))
      | _ -> known_literals in
    let known_relations =
      match old_clause with
      | Some (Data.App (old_fn, _)) ->
        StringMultiSet.remove model.known_relations old_fn
      | _ -> model.known_relations in
    let known_relations =
      match new_clause with
      | Some (Data.App (new_fn, _)) when not (String.is_empty new_fn) ->
        StringMultiSet.add known_relations new_fn
      | _ -> known_relations in
    {model with known_literals; known_relations}

  let update_max_time ~time model =
    let time = max time 0 in
    {model with max_time = time}

  let remove_fixed_clause ~index model =
    let old_clause, fixed_clauses = Utils.take_nth index model.fixed_clauses in
    let old_clause = Option.map ~f:(fun (_,_,cl)->cl) old_clause in
    let model = update_cached ?old_clause model in
    {model with fixed_clauses}

  let remove_property ~index model =
    let old_clause, properties = Utils.take_nth index model.properties in
    let model = update_cached ?old_clause model in
    {model with properties}

  let update_fixed_clause_time ~index ~time model =
    let time = max time 0 in
    {model with fixed_clauses=
                  Utils.update_nth_with index model.fixed_clauses
                    (fun (_,happens,clause) -> (time,happens,clause))}

  let update_fixed_clause_happens ~index ~happens model =
    {model with fixed_clauses=
                  Utils.update_nth_with index model.fixed_clauses
                    (fun (time,_,clause) -> (time,happens,clause))}


  let update_fixed_clause_value ~index clause model =
    let clause = normalize_clause clause in
    let old_clause = ref clause in
    let fixed_clauses =
      Utils.update_nth_with index model.fixed_clauses
        (fun (time,happens,old_clause_vl) ->
           old_clause := old_clause_vl;
           (time,happens,clause)) in
    let model = update_cached ~old_clause:!old_clause ~new_clause:clause model in
    {model with fixed_clauses}

  let update_property_value ~index clause model =
    let clause = normalize_clause clause in
    let old_clause = ref clause in
    let properties =
      Utils.update_nth_with index model.properties
        (fun old_clause_vl ->
           old_clause := old_clause_vl;
           clause) in
    let model = update_cached ~old_clause:!old_clause ~new_clause:clause model in
    {model with properties}

  let add_fixed_clause ~time clause model =
    let time = max time 0 in
    let clause = normalize_clause clause in
    let model = update_cached ~new_clause:clause model in
    {model with fixed_clauses=model.fixed_clauses @ [time,false,clause]}

  let add_property clause model =
    let clause = normalize_clause clause in
    let model = update_cached ~new_clause:clause model in
    {model with properties=model.properties @ [clause]}

  let add_error error model =
    {model with errors = model.errors @ [error]}

  let remove_error ~index model =
    {model with errors = Utils.drop_nth index model.errors}

  let add_rule rule model =
    let model =
      match rule with Rule.{
        head;
        properties;
        preconditions;
        initiated_fluents;
        terminated_fluents; _
      } ->
        let model = update_cached ~new_clause:head model in
        let model =
          List.fold_left ~init:model ~f:(fun model new_clause ->
            update_cached ~new_clause model 
          ) (properties
             @ (List.map ~f:(fun (_,_,cl) -> cl) preconditions)
             @ initiated_fluents
             @ terminated_fluents
            ) in
        model in
    {model with rules=model.rules @ [rule]}

  let remove_rule ~rule_index model =
    let old_rule, rules = Utils.take_nth rule_index model.rules in
    let model =
      match old_rule with
        None -> model
      | Some {
        head;
        properties;
        preconditions;
        initiated_fluents;
        terminated_fluents; _
      } ->
        let model = update_cached ~old_clause:head model in
        let model =
          List.fold_left ~init:model ~f:(fun model old_clause ->
            update_cached ~old_clause model 
          ) (properties
             @ (List.map ~f:(fun (_,_,cl) -> cl) preconditions)
             @ initiated_fluents
             @ terminated_fluents
            ) in
        model in
    {model with rules}

  let set_rule_head_clause ~rule_index clause model =
    let old_clause = ref None in
    let rules =
      Utils.update_nth_with rule_index model.rules (fun rule ->
        old_clause := Some (rule.Rule.head);
        Rule.set_head_clause clause rule
      ) in
    let model = update_cached ?old_clause:!old_clause ~new_clause:clause model in
    {model with rules}

  (* PROPERTY *)
  let add_rule_property ~rule_index clause model =
    let rules = 
      Utils.update_nth_with rule_index model.rules (fun rule ->
        Rule.add_property clause rule) in
    let model = update_cached ~new_clause:clause model in
    {model with rules}

  let update_rule_nth_property
        ~rule_index ~index clause model =
    let old_clause = ref None in
    let rules = Utils.update_nth_with rule_index model.rules (fun rule ->
      let old_clause', rule =
        Rule.update_nth_property ~index clause rule in
      old_clause := old_clause';
      rule
    ) in
    let model =
      update_cached ?old_clause:!old_clause ~new_clause:clause model in
    {model with rules}

  let remove_rule_nth_property
        ~rule_index ~index model =
    let old_clause = ref None in
    let rules = Utils.update_nth_with rule_index model.rules (fun rule ->
      let old_clause', rule = Rule.remove_nth_property ~index rule in
      old_clause := old_clause';
      rule
    ) in
    let model = update_cached ?old_clause:!old_clause model in
    {model with rules}

  (* INITIATED FLUENTS *)
  let add_rule_initiated_fluent ~rule_index clause model =
    let rules = 
      Utils.update_nth_with rule_index model.rules (fun rule ->
        Rule.add_initiated_fluent clause rule) in
    let model = update_cached ~new_clause:clause model in
    {model with rules}

  let update_rule_nth_initiated_fluent
        ~rule_index ~index clause model =
    let old_clause = ref None in
    let rules = Utils.update_nth_with rule_index model.rules (fun rule ->
      let old_clause', rule =
        Rule.update_nth_initiated_fluent ~index clause rule in
      old_clause := old_clause';
      rule
    ) in
    let model =
      update_cached ?old_clause:!old_clause ~new_clause:clause model in
    {model with rules}

  let remove_rule_nth_initiated_fluent
        ~rule_index ~index model =
    let old_clause = ref None in
    let rules = Utils.update_nth_with rule_index model.rules (fun rule ->
      let old_clause', rule = Rule.remove_nth_initiated_fluent ~index rule in
      old_clause := old_clause';
      rule
    ) in
    let model = update_cached ?old_clause:!old_clause model in
    {model with rules}

  (* TERMINATED FLUENTS *)
  let add_rule_terminated_fluent ~rule_index clause model =
    let rules = 
      Utils.update_nth_with rule_index model.rules (fun rule ->
        Rule.add_terminated_fluent clause rule) in
    let model = update_cached ~new_clause:clause model in
    {model with rules}

  let update_rule_nth_terminated_fluent
        ~rule_index ~index clause model =
    let old_clause = ref None in
    let rules = Utils.update_nth_with rule_index model.rules (fun rule ->
      let old_clause', rule =
        Rule.update_nth_terminated_fluent ~index clause rule in
      old_clause := old_clause';
      rule
    ) in
    let model =
      update_cached ?old_clause:!old_clause ~new_clause:clause model in
    {model with rules}

  let remove_rule_nth_terminated_fluent
        ~rule_index ~index model =
    let old_clause = ref None in
    let rules = Utils.update_nth_with rule_index model.rules (fun rule ->
      let old_clause', rule = Rule.remove_nth_terminated_fluent ~index rule in
      old_clause := old_clause';
      rule
    ) in
    let model = update_cached ?old_clause:!old_clause model in
    {model with rules}

  (* PRECONDITION *)
  let add_rule_precondition ~rule_index ~time ~negated clause model =
    let rules = 
      Utils.update_nth_with rule_index model.rules (fun rule ->
        Rule.add_precondition (time, negated, clause) rule) in
    let model = update_cached ~new_clause:clause model in
    {model with rules}

  let update_rule_nth_precondition_value
        ~rule_index ~index clause model =
    let old_clause = ref None in
    let rules = Utils.update_nth_with rule_index model.rules (fun rule ->
      let old_clause', rule =
        Rule.update_nth_precondition_value ~index clause rule in
      old_clause := old_clause';
      rule
    ) in
    let model =
      update_cached ?old_clause:!old_clause ~new_clause:clause model in
    {model with rules}

  let update_rule_nth_precondition_time
        ~rule_index ~index time model =
    let rules =
      Utils.update_nth_with rule_index model.rules
        (Rule.update_nth_precondition_time ~index time) in
    {model with rules}

  let update_rule_nth_precondition_negated
        ~rule_index ~index negated model =
    let rules =
      Utils.update_nth_with rule_index model.rules
        (Rule.update_nth_precondition_negated ~index negated) in
    {model with rules}


  let remove_rule_nth_precondition
        ~rule_index ~index model =
    let old_clause = ref None in
    let rules = Utils.update_nth_with rule_index model.rules (fun rule ->
      let old_clause', rule = Rule.remove_nth_precondition ~index rule in
      old_clause := old_clause';
      rule
    ) in
    let model =
      update_cached ?old_clause:(Option.map
                                   ~f:(fun (_,_,cl)->cl)
                                   !old_clause) model in
    {model with rules}

  let update_timelines data model =
    {model with  model=Some (of_model data)}

  let default ?(fixed_clauses=[]) ?(properties=[]) () =
    let model = empty in
    let model = 
      List.fold_left
        ~init:model
        ~f:(fun model (time, clause) ->
          add_fixed_clause ~time clause model)
        fixed_clauses in
    let model = 
      List.fold_left
        ~init:model
        ~f:(fun model clause ->
          add_property clause model)
        properties in
    model

end

module Action = struct

  type t =
    | AddError of string
    | RemoveError of int
    | SetMaxTime of string
    | RemoveFixedClause of int
    | SetFixedClauseTime of {index: int; time: string}
    | SetFixedClauseHappens of {index: int; happens: bool}
    | SetFixedClauseValue of {index: int; clause: Data.clause}
    | AddFixedClause of {time: string; clause: Data.clause }
    | RemoveProperty of int
    | SetPropertyValue of { index: int; clause: Data.clause }
    | AddProperty of Data.clause 
    | SetState of Data.problem_spec

    | AddRule of Rule.t
    | RemoveRule of int
    | SetRuleHeadClause of {index: int; clause: Data.clause}
    | AddRuleProperty of {index: int; clause: Data.clause}
    | UpdateRuleNthProperty of {
        index: int; property_index: int; clause: Data.clause
      }
    | RemoveRuleNthProperty of {
        index: int; property_index: int
      } 
    | AddRuleInitiatedFluent of { index: int; clause: Data.clause }
    | UpdateRuleNthInitiatedFluent of {
        index: int; fluent_index: int; clause: Data.clause
      }
    | RemoveRuleNthInitiatedFluent of {
        index: int; fluent_index: int;
      }
    | AddRuleTerminatedFluent of { index: int; clause: Data.clause }
    | UpdateRuleNthTerminatedFluent of  {
        index: int; fluent_index: int; clause: Data.clause
      }
    | RemoveRuleNthTerminatedFluent of {
        index: int; fluent_index: int;
      }
    | AddRulePrecondition of {
        index: int;
        time: int;
        negated: bool;
        clause: Data.clause;
      }
    | UpdateRuleNthPreconditionValue of {
        index: int;
        precondition_index: int;
        clause: Data.clause;
      } 
    | UpdateRuleNthPreconditionTime of {
        index: int;
        precondition_index: int;
        time: string;
      }
    | UpdateRuleNthPreconditionNegated of {
        index: int;
        precondition_index: int;
        negated: bool;
      }
    | RemoveRuleNthPrecondition of {
        index: int;
        precondition_index: int;
      }
    | UpdateTimelines of Data.model
  [@@deriving sexp]

end


