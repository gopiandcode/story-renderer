function writeData(data) {
  var opts = {
    types: [{
      description: 'Story renderer save file',
      accept: {'application/x-binary': ['.sdb']},
    }],
  };
  window.showSaveFilePicker(opts).then(function (fhandle) {
      fhandle.createWritable().then(function (writeable) {
          writable.write(data).then(function (_) {
              writeable.close();
          })
      })
  });
}

globalThis.upload_file = function readData(file,then) {
    var reader = new FileReader();
    reader.addEventListener("load", function () {
        then(reader.result);
    }, false);
    reader.readAsText(file);
}


globalThis.download_file = function download_file(dataBlob, fileName) {
    var aElement = document.createElement('a');
    aElement.setAttribute('download', fileName);
    var href = URL.createObjectURL(dataBlob);
    aElement.href = href;
    aElement.setAttribute('target', '_blank');
    aElement.click();
    URL.revokeObjectURL(href);
    return;
}

globalThis.download_string = function download_string(text, fileName) {
    var aElement = document.createElement('a');
    aElement.setAttribute('download', fileName);
    var dataBlob = new Blob([text], {
        type: "text/plain",
    });
    var href = URL.createObjectURL(dataBlob);
    aElement.href = href;
    aElement.setAttribute('target', '_blank');
    aElement.click();
    URL.revokeObjectURL(href);
    return;
}

