open! Core
open! Bonsai_web

module IntMap = Map.Make (Int)
module StringMap = Map.Make (String)
module StringSet = Set.Make (String)

let drop_nth n ls =
  let rec loop original acc i ls =
    match i, ls with
    | 0, _ :: t -> List.rev_append acc t
    | n, h :: t when n > 0 -> loop original (h :: acc) (n - 1) t
    | _, [] -> original
    | _ -> original in
  loop ls [] n ls

let take_nth n ls =
  let rec loop original acc i ls =
    match i, ls with
    | 0, dropped :: t -> Some dropped, List.rev_append acc t
    | n, h :: t when n > 0 -> loop original (h :: acc) (n - 1) t
    | _, [] -> None, original
    | _ -> None, original in
  loop ls [] n ls


let update_nth n ls vl =
  let rec loop original acc i vl ls =
    match i, ls with
    | 0, _ :: t -> List.rev_append acc (vl :: t)
    | n, h :: t when n > 0 -> loop original (h :: acc) (n - 1) vl t
    | _, [] -> List.rev_append acc [vl]
    | _ -> vl :: original in
  loop ls [] n vl ls

let update_nth_returning n ls vl =
  let rec loop original acc i vl ls =
    match i, ls with
    | 0, old :: t -> Some old, List.rev_append acc (vl :: t)
    | n, h :: t when n > 0 -> loop original (h :: acc) (n - 1) vl t
    | _, [] -> None, List.rev_append acc [vl]
    | _ -> None, vl :: original in
  loop ls [] n vl ls

let update_nth_with n ls f =
  let rec loop original acc i f ls =
    match i, ls with
    | 0, h :: t -> List.rev_append acc (f h :: t)
    | n, h :: t when n > 0 -> loop original (h :: acc) (n - 1) f t
    | _, [] -> original
    | _ -> original in
  loop ls [] n f ls

let update_nth_returning_with n ls f =
  let rec loop original acc i f ls =
    match i, ls with
    | 0, h :: t -> Some h, List.rev_append acc (f h :: t)
    | n, h :: t when n > 0 -> loop original (h :: acc) (n - 1) f t
    | _, [] -> None, original
    | _ -> None, original in
  loop ls [] n f ls

let mapM ~f ls =
  let elts = Value.map ls ~f:(fun ls ->
      IntMap.of_iteri_exn ~iteri:(fun ~f ->
          List.iteri ~f:(fun key data -> f ~key ~data) ls
        )) in
  let elts = Bonsai.assoc (module IntMap.Key) elts ~f
             |> Computation.map ~f:(fun map ->
                 Map.to_sequence ~order:`Increasing_key map
                 |> Sequence.map ~f:(snd)
                 |> Sequence.to_list
               ) in
  elts


module StringMultiSet : sig
  type t [@@deriving sexp, equal]

  val empty : t

  val of_set: StringSet.t -> t

  val remove : t -> string -> t

  val add : t -> string -> t

  val matches : t -> pattern:string -> string list

  val union: t -> t -> t

end = struct

  type t = int StringMap.t
  [@@deriving sexp, equal]

  let of_set set = StringMap.of_iteri_exn  ~iteri:(fun ~f ->
      StringSet.iter set ~f:(fun key -> f ~key ~data:1))
  
  let empty : t = StringMap.empty

  let union l r =
    StringMap.merge l r ~f:(fun ~key:_ -> function
        | `Left c
        | `Right c when c > 0 -> Some c
        | `Both (l,r) when (max l 0) + (max r 0) > 0 ->
          Some ((max l 0) + (max r 0))
        | _ -> None
      ) 

  let remove (map: t) vl : t =
    StringMap.change map vl ~f:(function
        | Some n when n > 1 -> Some (n - 1)
        | _ -> None
      )

  let add (map: t) vl : t =
    StringMap.change map vl ~f:(function
        | Some n -> Some (n + 1)
        | None -> Some 1
      )

  let matches (map: t) ~pattern =
    let is_match s =
      Fuzzy_match.is_match ~char_equal:Char.Caseless.equal ~pattern s in
    StringMap.to_sequence map
    |> Sequence.filter ~f:(fun (txt, _) -> is_match txt)
    |> Sequence.to_list
    |> List.sort ~compare:(fun (_, count) (_, count') ->
        Int.compare count count')
    |> List.map ~f:fst

end

let download_file =
  let open Js_of_ocaml in
  let download_file:
    File.blob Js.t -> Js.js_string Js.t -> unit =
    Js.Unsafe.get Js.Unsafe.global "download_file" in
  fun ?(default_name="download") data ->
    download_file data (Js.string default_name)

let download_string =
  let open Js_of_ocaml in
  let download_string:
    Js.js_string Js.t -> Js.js_string Js.t -> unit =
    Js.Unsafe.get Js.Unsafe.global "download_string" in
  fun ?(default_name="download") data ->
    download_string data (Js.string default_name)
