open! Core

let solve_def (problem: Story_renderer_data.problem_spec) =
  let data =
    [%sexp_of: Story_renderer_data.problem_spec] problem
    |> Sexp.to_string_mach in
  Async_kernel.Deferred.map
    ~f:(fun s ->
        Or_error.bind ~f:(fun s ->
            [%of_sexp: (Story_renderer_data.model, string) Result.t] @@
            Sexp.of_string s
            |> Result.map_error ~f:(Error.of_string)
      ) s)
    (Async_js.Http.post
       ~body:(String data)
       "/api/solve")

let save_def (problem: Story_renderer_data.problem_spec) :
  Js_of_ocaml.File.blob Js_of_ocaml.Js.t
    Or_error.t Async_kernel.Deferred.t =
  let data =
    [%sexp_of: Story_renderer_data.problem_spec] problem
    |> Sexp.to_string_mach in
  Async_kernel.Deferred.map ~f:(fun s ->
      Or_error.bind ~f:(fun s ->
          match Js_of_ocaml.Js.Opt.to_option s.content with
          | None -> Or_error.error_string "Empty response from server"
          | Some data -> Or_error.return data
        ) s
    )
    Async_js.Http.(
      request ~url:"/api/save" ~response_type:Response_type.Blob
        (Method_with_args.Post (Some (Post_body.String data)))
    ) 

let load_def data =
  Async_kernel.Deferred.map
    ~f:(fun s ->
        Or_error.map ~f:(fun s ->
            [%of_sexp: Story_renderer_data.problem_spec] @@
            Sexp.of_string s
          ) s)
    (Async_js.Http.post
       ~body:(Blob data)
       "/api/load")

let solve =
  Bonsai_web.Effect.of_deferred_fun solve_def

let save =
  Bonsai_web.Effect.of_deferred_fun save_def

let load =
  Bonsai_web.Effect.of_deferred_fun load_def
