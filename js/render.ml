open! Core
open! Bonsai_web
open! Bonsai.Let_syntax

module StringMultiSet = Utils.StringMultiSet

module VN = Vdom.Node
module VA = Vdom.Attr

let timeline_event_marker n =
  VN.div ~attr:(VA.classes ["timeline-event"]) [
    VN.span ~attr:(VA.create "data-time" (Int.to_string n)) [];
  ]

let timeline_span_marker ~max_time
    ?header ?(color=Data.Red) ?(start=0) ?(stop=max_time) () =
  VN.div ~attr:(VA.classes ["timeline-row"])
    ([
      VN.div
        ~attr:(VA.class_ "timeline-header") [
        VN.span (Option.to_list (Option.map ~f:VN.text header))
      ]
    ] @ List.init (max_time)
        ~f:(fun i ->
            let attr = (VA.create "data-time" (Int.to_string i)) in
            let attr =
              if i = start
              then VA.many_without_merge [
                  attr;
                  VA.create "data-event"
                    (String.init 5
                       ~f:(fun _ ->
                           Random.int_incl
                             (Char.to_int 'a')
                             (Char.to_int 'z')
                           |> Char.of_int_exn
                         ))]
              else attr in
            let class' = 
              if start < i && i < stop
              then Some "timeline-event-in-progress"
              else if i = start
              then Some "timeline-event-start"
              else if i = stop
              then Some "timeline-event-stop"
              else None in
            VN.div
              ~attr:(VA.many_without_merge [
                  VA.classes (["timeline-spacer"] @ Option.to_list class' @
                              [Data.to_color_class color])
                ]) [
              VN.span ~attr []
            ]
          ))

let event_to_div ~max_time ~color i evt =
  let attr _nme = VA.(
      create "data-time" (Int.to_string i) (* @
       * create "data-name" nme *)
    ) in
  let data =
    match evt with
    | Data.State.Region { start; stop; clause } ->
      let start = Option.value start ~default:0 in
      let stop = Option.value stop ~default:max_time in
      let nme = (Format.asprintf "%a" Story_renderer_data.pp_clause clause) in
      let attr = attr nme in
      (* let attr = if i = start
       *   then VA.many_without_merge [ attr; VA.create "data-event" nme ]
       *   else attr in *)
      if start <= i && i < stop then
        let class', nme =
          if i = start
          then ["timeline-event-start"], Some nme
          else if i = stop
          then ["timeline-event-stop"], None
          else ["timeline-event-in-progress"], None in
        Some (attr, class', nme)
      else None
    | Event { time; clause } ->
      let nme = (Format.asprintf "%a" Story_renderer_data.pp_clause clause) in
      let attr = attr nme in
      (* let attr = VA.many_without_merge [ attr; VA.create "data-event" nme ] in *)
      if time = i
      then Some (attr, ["timeline-event-singleton"], Some nme)
      else None in
  match data with
  | Some (attr, class', nme) ->
    Some (VN.div
            ~attr:(VA.many_without_merge [
                VA.classes (
                  ["timeline-spacer"] @ class' @ [Data.to_color_class color]
                )
              ]) [VN.span ~attr (match nme with
              | Some nme -> [
                  VN.div ~attr:(VA.class_ "timeline-popup") [VN.p [VN.text nme]]
                ]
              | None -> [])])
  | None ->
    None

let render_timeline_row ~max_time ({header;events=ls}: Data.State.timeline_row)  =
  VN.div ~attr:(VA.classes ["timeline-row"])
    ([
      VN.div
        ~attr:(VA.class_ "timeline-header") [
        VN.span [VN.text header]
      ]
    ] @ List.init (max_time)
        ~f:(fun i ->
            let div = List.find_map ls
                ~f:(fun (color, evt) ->
                    event_to_div ~max_time ~color i evt) in
            match div with
            | Some div -> div
            | None ->
              let attr = (VA.create "data-time" (Int.to_string i)) in
              VN.div ~attr:(VA.classes (["timeline-spacer"])) [
                VN.span ~attr []
              ]
          ))

let render_options_panel set_max_time max_time =
  let%arr max_time = max_time
  and set_max_time = set_max_time in
  VN.div ~attr:(VA.class_ "panel-options") [
    VN.label ~key:"max-time-label" [VN.text "Max time:"];
    VN.input ~key:"max-time-input" ~attr:(VA.many_without_merge [
        VA.type_ "number";
        VA.on_change (fun _ text -> set_max_time text);
        VA.value (Int.to_string max_time)
      ]) [];
  ]

let render_clause set_clause ~known_relations ~known_literals
    (clause : Story_renderer_data.clause Value.t) =
  let%sub (App (fn, args)) = Bonsai.read clause in
  let%sub args_view =
    Utils.mapM ~f:(fun ind arg ->
        let%sub autocomplete_input =
          Components.autocomplete_box
            ~attr:(VA.class_ "editable-clause-input")
            (Value.map4 fn ind args set_clause
               ~f:(fun fn ind args set_clause arg ->
                   set_clause (Story_renderer_data.App (fn, Utils.update_nth ind args arg))
                 )
            ) known_literals arg in
        let%arr ind and fn and args
        and set_clause and autocomplete_input in
        VN.span ~attr:(VA.class_ "editable-clause-argument") [
          autocomplete_input;
          (* VN.input ~attr:VA.( *)
          (*     value arg @ *)
          (*     create "size" (Int.to_string @@ String.length arg) @ *)
          (*     type_ "text" @ *)
          (*     on_change (fun _ arg -> *)
          (*         set_clause (Data.App (fn, update_nth ind args arg))) *)
          (*   ) []; *)
          VN.button ~attr:VA.(
              class_ "editable-clause-remove-button" @
              on_click (fun _ ->
                  set_clause (App (fn, Utils.drop_nth ind args))
                )
            ) [VN.text "✕"]
        ]
      ) args in
  let%sub autocomplete_input =
    Components.autocomplete_box
      ~attr:(VA.class_ "editable-clause-input")
      (Value.map2 args set_clause
         ~f:(fun args set_clause fn ->
             set_clause (App (fn, args))
           )
      ) known_relations fn in
  let%arr fn
  and args
  and set_clause
  and args_view
  and autocomplete_input in
  VN.div ~attr:(VA.class_ "editable-clause") [
    VN.span ~attr:VA.(
        classes ["editable-clause-argument"; "editable-clause-head"]
      ) (List.concat [
        [
          autocomplete_input
        ];
        [VN.text "("];
        (List.intersperse ~sep:(VN.text ",") args_view);
        [VN.button ~attr:VA.(
             class_ "editable-clause-add-argument" @
             on_click (fun _ -> set_clause (App (fn, List.concat [args; [""]])))
           ) [VN.text "+"]];
        [VN.text ")"];
      ])
  ]

let render_error_panel ~remove_error errors =
  let%sub error_list =
    Utils.mapM ~f:(fun ind error ->
        let%arr ind and error and remove_error in
        VN.div ~attr:(VA.class_ "error-notification") [
          VN.span [VN.text error];
          VN.button ~attr:(VA.on_click (fun _ -> remove_error ind)) [VN.text "✖"];
        ]
      ) errors in
  let%arr error_list in
  VN.div ~attr:(VA.class_ "error-panel") error_list

let render_events_grid ~remove_nth_event ~add_new_event
    ~update_event_time ~update_event_happens ~update_clause
    ~known_relations ~known_literals ls =
  let grid_elts = Utils.mapM ~f:(fun ind (data: (int * bool * Story_renderer_data.clause) Value.t) ->
      let set_clause = Value.map2 ind update_clause
          ~f:(fun ind update_clause clause ->
              update_clause ind clause
            ) in
      let%sub clause_view =
        render_clause
          set_clause ~known_literals ~known_relations
          (Value.map ~f:(fun (_, _,cl) -> cl) data) in
      let%sub happens_button =
        let%arr negated =
          Value.map ~f:(fun (_,negated,_) -> negated) data
        and update_event_happens
        and ind in
        let attr = VA.classes
                     (if negated
                      then ["precondition-negated"; "precondition-negated-active"]
                      else ["precondition-negated"; "precondition-negated-inactive"])
        in
        VN.button ~attr:VA.(
          attr @
          on_click (fun _ -> update_event_happens ind (not negated))
        ) [
          VN.text "happens"
        ] in

      let%arr (time, _, _) = data
      and ind
      and update_event_time
      and remove_nth_event
      and clause_view
      and happens_button in
      [
        VN.div ~attr:(VA.class_ "panel-grid-cell") [
          VN.input ~attr:VA.(VA.type_ "number" @ VA.value (Int.to_string time) @
                             VA.on_change (fun _ vl ->
                               update_event_time ind vl
                             )
                            ) []
        ];
        VN.div ~attr:(VA.class_ "panel-grid-cell") [
            happens_button
          ];
        VN.div ~attr:(VA.class_ "panel-grid-cell") [
          clause_view
        ];
        VN.div ~attr:(VA.class_ "panel-grid-cell") [
          VN.button ~attr:VA.(class_ "fixed-events-remove-element" @
                              on_click (fun _ -> remove_nth_event ind))
            [VN.text "✖"]
        ];
      ]
    ) ls in
  let%sub add_button =
    let%arr add_new_event in
    VN.div ~attr:(VA.classes ["fixed-events-grid-cell-button";
                              "panel-grid-cell-button"]) [
      VN.button ~attr:(VA.on_click (fun _ ->
          add_new_event
        )) [VN.text "+"]
    ] in
  let%sub grid_elts = grid_elts in
  let%arr grid_elts
  and add_button in
  VN.div ~attr:(VA.class_ "fixed-events-grid") (List.concat ([
      [
        VN.div ~attr:(VA.class_ "panel-grid-header") [
          VN.span [VN.text "Time"];
        ];
        VN.div ~attr:(VA.class_ "panel-grid-header") [
          VN.span [VN.text "happens?"];
        ];
        VN.div ~attr:(VA.class_ "panel-grid-header") [
          VN.span [VN.text "Clause"];
        ];
        VN.div ~attr:(VA.class_ "panel-grid-header") [];
      ]
    ] @ grid_elts @ [[add_button]]))

let render_preconditions_grid ~remove_nth_event ~add_new_event
    ~update_event_time
    ~update_event_negated
    ~update_clause
    ~known_relations ~known_literals ls =
  let grid_elts =
    Utils.mapM ~f:(fun ind (data: (int * bool * Story_renderer_data.clause) Value.t) ->
        let set_clause = Value.map2 ind update_clause
            ~f:(fun ind update_clause clause ->
                update_clause ind clause
              ) in
        let%sub clause_view =
          render_clause
            set_clause ~known_literals ~known_relations
            (Value.map ~f:(fun (_,_,cl) -> cl) data) in
        let%sub negated_button =
          let%arr negated =
            Value.map ~f:(fun (_,negated,_) -> negated) data
          and update_event_negated
          and ind in
          let attr = VA.classes
              (if negated
               then ["precondition-negated"; "precondition-negated-active"]
               else ["precondition-negated"; "precondition-negated-inactive"])
          in
          VN.button ~attr:VA.(
              attr @
              on_click (fun _ -> update_event_negated ind (not negated))
            ) [
            VN.text "not"
          ] in
        let%arr (time, _, _clause) = data
        and ind
        and update_event_time
        and remove_nth_event
        and clause_view
        and negated_button in
        [
          VN.div ~attr:(VA.class_ "panel-grid-cell") [
            VN.input ~attr:VA.(VA.type_ "number" @ VA.value (Int.to_string time) @
                               VA.on_change (fun _ vl ->
                                   update_event_time ind vl
                                 )
                              ) []
          ];
          VN.div ~attr:(VA.class_ "panel-grid-cell") [
            negated_button
          ];
          VN.div ~attr:(VA.class_ "panel-grid-cell") [
            clause_view
          ];
          VN.div ~attr:(VA.class_ "panel-grid-cell") [
            VN.button ~attr:VA.(class_ "fixed-events-remove-element" @
                                on_click (fun _ -> remove_nth_event ind))
              [VN.text "✖"]
          ];
        ]
      ) ls in
  let%sub add_button =
    let%arr add_new_event in
    VN.div ~attr:(VA.classes ["preconditions-grid-cell-button";
                              "panel-grid-cell-button"]) [
      VN.button ~attr:(VA.on_click (fun _ ->
          add_new_event
        )) [VN.text "+"]
    ] in
  let%sub grid_elts = grid_elts in
  let%arr grid_elts
  and add_button in
  VN.div ~attr:(VA.class_ "preconditions-grid") (List.concat ([
      [
        VN.div ~attr:(VA.class_ "panel-grid-header") [
          VN.span [VN.text "Offset"];
        ];
        VN.div ~attr:(VA.class_ "panel-grid-header") [
          VN.span [VN.text "not?"];
        ];
        VN.div ~attr:(VA.class_ "panel-grid-header") [
          VN.span [VN.text "Clause"];
        ];
        VN.div ~attr:(VA.class_ "panel-grid-header") [];
      ]
    ] @ grid_elts @ [[add_button]]))

let render_properties_grid ~remove_nth_property ~add_new_property
    ~update_clause
    ~known_relations ~known_literals ls =
  let grid_elts =
    Utils.mapM ~f:(fun ind (data: Story_renderer_data.clause Value.t) ->
        let set_clause = Value.map2 ind update_clause
            ~f:(fun ind update_clause clause ->
                update_clause ind clause
              ) in
        let%sub clause_view =
          render_clause
            set_clause ~known_literals ~known_relations
            data in
        let%arr ind
        and remove_nth_property
        and clause_view in
        [
          VN.div ~attr:(VA.class_ "panel-grid-cell") [
            clause_view
          ];
          VN.div ~attr:(VA.class_ "panel-grid-cell") [
            VN.button ~attr:VA.(class_ "fixed-events-remove-element" @
                                on_click (fun _ -> remove_nth_property ind))
              [VN.text "✖"]
          ];
        ]
      ) ls in
  let%sub add_button =
    let%arr add_new_property in
    VN.div ~attr:(VA.classes ["property-grid-cell-button";
                              "panel-grid-cell-button"]) [
      VN.button ~attr:(VA.on_click (fun _ ->
          add_new_property
        )) [VN.text "+"]
    ] in
  let%sub grid_elts = grid_elts in
  let%arr grid_elts and add_button in
  VN.div ~attr:(VA.class_ "property-grid") (List.concat ([
      [
        VN.div ~attr:(VA.class_ "panel-grid-header") [
          VN.span [VN.text "Clause"];
        ];
        VN.div ~attr:(VA.class_ "panel-grid-header") [];
      ]
    ] @ grid_elts @ [[add_button]]))

let render_rule_box ~remove_rule ~set_head_clause
    ~remove_nth_rule_property
    ~update_nth_rule_property
    ~add_rule_property

    ~remove_nth_rule_event
    ~update_nth_rule_event_value
    ~update_nth_rule_event_time
    ~update_nth_rule_event_negated
    ~add_rule_event

    ~remove_nth_rule_initiated_fluent
    ~update_nth_rule_initiated_fluent
    ~add_rule_initiated_fluent

    ~remove_nth_rule_terminated_fluent
    ~update_nth_rule_terminated_fluent
    ~add_rule_terminated_fluent

    ~known_relations ~known_literals rule =
  let%sub rule_variables =
    return (Value.map ~f:Data.Rule.variables rule) in
  let%sub rule_properties =
    return (Value.map ~f:Data.Rule.properties rule) in
  let%sub rule_events =
    return (Value.map ~f:Data.Rule.preconditions rule) in
  let%sub rule_initiated_fluents =
    return (Value.map ~f:Data.Rule.initiated_fluents rule) in
  let%sub rule_terminated_fluents =
    return (Value.map ~f:Data.Rule.terminated_fluents rule) in
  let%sub known_literals =
    return @@
    Value.map2 ~f:(StringMultiSet.union)
      known_literals rule_variables in
  let%sub head_clause =
    render_clause set_head_clause
      ~known_relations
      ~known_literals
      (Value.map ~f:Data.Rule.head rule) in
  let%sub rule_properties =
    render_properties_grid
      ~remove_nth_property:remove_nth_rule_property
      ~add_new_property:add_rule_property
      ~update_clause:update_nth_rule_property
      ~known_relations ~known_literals
      rule_properties in
  let%sub rule_initiated_fluents =
    render_properties_grid
      ~remove_nth_property:remove_nth_rule_initiated_fluent
      ~add_new_property:add_rule_initiated_fluent
      ~update_clause:update_nth_rule_initiated_fluent
      ~known_relations ~known_literals
      rule_initiated_fluents in
  let%sub rule_terminated_fluents =
    render_properties_grid
      ~remove_nth_property:remove_nth_rule_terminated_fluent
      ~add_new_property:add_rule_terminated_fluent
      ~update_clause:update_nth_rule_terminated_fluent
      ~known_relations ~known_literals
      rule_terminated_fluents in
  let%sub rule_events =
    render_preconditions_grid
      ~remove_nth_event:remove_nth_rule_event
      ~add_new_event:add_rule_event
      ~update_clause:update_nth_rule_event_value
      ~update_event_time:update_nth_rule_event_time
      ~update_event_negated:update_nth_rule_event_negated
      ~known_relations ~known_literals
      rule_events in
  let%arr remove_rule
  and head_clause
  and rule_properties and rule_events
  and rule_initiated_fluents and rule_terminated_fluents in
  VN.(div ~attr:(VA.class_ "rule-box") [
      div ~attr:(VA.class_ "rule-delete-button") [
        button ~attr:(VA.on_click (fun _ -> remove_rule)) [text "✖"];
      ];
      div ~attr:(VA.class_ "rule-head") [
        span ~attr:(VA.class_ "rule-head-text") [
          text "Clause:"
        ];
        div ~attr:(VA.class_ "rule-head-value") [
          span
            ~attr:(VA.classes ["rule-head-time"; "rule-time-variable"])
            [
              text "T"
            ];
          head_clause
        ]
      ];
      div ~attr:(VA.class_ "rule-split-panel") [
        div ~attr:(VA.classes ["rule-properties";"rule-panel"]) [
          div ~attr:(VA.classes ["rule-panel-header"]) [
            h2 [text "Properties"];
          ];
          div ~attr:(VA.classes ["rule-panel-contents"]) [
            rule_properties
          ]
        ];
        div ~attr:(VA.classes ["rule-preconditions";"rule-panel"]) [
          div ~attr:(VA.classes ["rule-panel-header"]) [
            h2 [text "Preconditions"];
          ];
          div ~attr:(VA.classes ["rule-panel-contents"]) [
            rule_events
          ]
        ];
      ];
      div ~attr:(VA.class_ "rule-split-panel") [
        div ~attr:(VA.classes ["rule-fluents";"rule-panel"]) [
          div ~attr:(VA.classes ["rule-panel-header"]) [
            h2 [text "Initiated Fluents"];
          ];
          div ~attr:(VA.classes ["rule-panel-contents"]) [
            rule_initiated_fluents
          ]
        ];
        div ~attr:(VA.classes ["rule-fluents";"rule-panel"]) [
          div ~attr:(VA.classes ["rule-panel-header"]) [
            h2 [text "Terminated Fluents"];
          ];
          div ~attr:(VA.classes ["rule-panel-contents"]) [
            rule_terminated_fluents
          ]
        ];
      ];
    ])  

let render_timeline ~max_time model =
  match%sub model with
  | None ->
    Computation.return
    VN.(div ~attr:(VA.classes ["timeline"; "timeline-contents"; "timeline-no-model"]) [
        span [text "No Model"]
      ])
  | Some model ->    
  let%arr max_time
  and model in
  let model = List.map ~f:(render_timeline_row ~max_time) model in
  VN.(div ~attr:(VA.classes ["timeline"; "timeline-contents"]) model)

let render_state run_action state =
  let%sub known_relations = return
      (Value.map ~f:Data.State.known_relations state) in
  let%sub known_literals =
    return (Value.map ~f:Data.State.known_literals state) in
  let%sub options_panel =
    let set_max_time = Value.map run_action
        ~f:(fun f time -> f (Data.Action.SetMaxTime time)) in
    render_options_panel set_max_time
      (Value.map ~f:Data.State.max_time state) in
  let%sub fixed_events_grid =
    let remove_nth_event = Value.map run_action
        ~f:(fun f index -> f (Data.Action.RemoveFixedClause index)) in
    let update_event_time = Value.map run_action
        ~f:(fun f index time ->
            f (Data.Action.SetFixedClauseTime {index;time})) in
    let update_event_happens = Value.map run_action
        ~f:(fun f index happens ->
            f (Data.Action.SetFixedClauseHappens {index;happens})) in
    let update_clause = Value.map run_action
        ~f:(fun f index clause ->
            f (Data.Action.SetFixedClauseValue {index;clause})) in
    let add_new_event = Value.map run_action
        ~f:(fun f -> f (Data.Action.AddFixedClause {
            time="0";
            clause=(App ("",[]))
          })) in
    render_events_grid ~remove_nth_event ~add_new_event
      ~update_event_time ~update_event_happens ~update_clause
      ~known_relations ~known_literals
      (Value.map ~f:Data.State.fixed_clauses state) in
  let%sub property_grid =
    let remove_nth_property = Value.map run_action
        ~f:(fun f index -> f (Data.Action.RemoveProperty index)) in
    let update_clause = Value.map run_action
        ~f:(fun f index clause ->
            f (Data.Action.SetPropertyValue {index;clause})) in
    let add_new_property = Value.map run_action
        ~f:(fun f -> f (Data.Action.AddProperty (App ("",[])))) in
    render_properties_grid ~remove_nth_property ~add_new_property
      ~update_clause
      ~known_relations ~known_literals
      (Value.map ~f:Data.State.properties state) in
  let%sub error_panel =
    let remove_error = Value.map run_action ~f:(fun f index -> f (Data.Action.RemoveError index)) in
    render_error_panel ~remove_error (Value.map ~f:Data.State.errors state) in
  let%sub rules_list =
    Utils.mapM (Value.map ~f:Data.State.rules state) ~f:(fun ind rule ->
        let remove_rule =
          Value.map2 run_action ind ~f:(fun f ind -> f (Data.Action.RemoveRule ind)) in
        let set_head_clause =
          Value.map2 run_action ind ~f:(fun f index clause ->
              f (Data.Action.SetRuleHeadClause {index;clause})
            ) in

        let remove_nth_rule_property =
          Value.map2 run_action ind ~f:(fun f index property_index ->
              f (Data.Action.RemoveRuleNthProperty {index; property_index})
            ) in
        let update_nth_rule_property =
          Value.map2 run_action ind ~f:(fun f index property_index clause ->
              f (Data.Action.UpdateRuleNthProperty {index; property_index; clause})
            ) in
        let add_rule_property =
          Value.map2 run_action ind ~f:(fun f index ->
              f (Data.Action.AddRuleProperty {index; clause=App("",[])})
            ) in

        let remove_nth_rule_initiated_fluent =
          Value.map2 run_action ind ~f:(fun f index fluent_index ->
              f (Data.Action.RemoveRuleNthInitiatedFluent {index; fluent_index})
            ) in
        let update_nth_rule_initiated_fluent =
          Value.map2 run_action ind ~f:(fun f index fluent_index clause ->
              f (Data.Action.UpdateRuleNthInitiatedFluent {index; fluent_index; clause})
            ) in
        let add_rule_initiated_fluent =
          Value.map2 run_action ind ~f:(fun f index ->
              f (Data.Action.AddRuleInitiatedFluent {index; clause=App("",[])})
            ) in

        let remove_nth_rule_terminated_fluent =
          Value.map2 run_action ind ~f:(fun f index fluent_index ->
              f (Data.Action.RemoveRuleNthTerminatedFluent {index; fluent_index})
            ) in
        let update_nth_rule_terminated_fluent =
          Value.map2 run_action ind ~f:(fun f index fluent_index clause ->
              f (Data.Action.UpdateRuleNthTerminatedFluent {index; fluent_index; clause})
            ) in
        let add_rule_terminated_fluent =
          Value.map2 run_action ind ~f:(fun f index ->
              f (Data.Action.AddRuleTerminatedFluent {index; clause=App("",[])})
            ) in

        let remove_nth_rule_event =
          Value.map2 run_action ind ~f:(fun f index precondition_index ->
              f (Data.Action.RemoveRuleNthPrecondition {index; precondition_index})
            ) in

        let update_nth_rule_event_value =
          Value.map2 run_action ind ~f:(fun f index precondition_index clause ->
              f (Data.Action.UpdateRuleNthPreconditionValue {index; precondition_index; clause})
            ) in
        let update_nth_rule_event_time =
          Value.map2 run_action ind ~f:(fun f index precondition_index time ->
              f (Data.Action.UpdateRuleNthPreconditionTime {index; precondition_index; time})
            ) in
        let update_nth_rule_event_negated =
          Value.map2 run_action ind ~f:(fun f index precondition_index negated ->
              f (Data.Action.UpdateRuleNthPreconditionNegated
                   {index; precondition_index; negated})
            ) in

        let add_rule_event =
          Value.map2 run_action ind ~f:(fun f index ->
              f (Data.Action.AddRulePrecondition {
                  index; time=0; negated=false;
                  clause=App ("",[])
                })
            ) in
        render_rule_box
          ~remove_rule
          ~set_head_clause

          ~remove_nth_rule_property
          ~update_nth_rule_property
          ~add_rule_property

          ~remove_nth_rule_event
          ~update_nth_rule_event_value
          ~update_nth_rule_event_time
          ~update_nth_rule_event_negated
          ~add_rule_event

          ~remove_nth_rule_initiated_fluent
          ~update_nth_rule_initiated_fluent
          ~add_rule_initiated_fluent

          ~remove_nth_rule_terminated_fluent
          ~update_nth_rule_terminated_fluent
          ~add_rule_terminated_fluent
          ~known_relations
          ~known_literals
          rule
      ) in
  let%sub timeline_contents =
    let max_time = Value.map ~f:Data.State.max_time state
    and model = Value.map ~f:Data.State.model state in
    render_timeline ~max_time model in
  let%sub rules_add_button =
    let%arr run_action in
    VN.div ~attr:(VA.class_ "rule-box-button") [
      VN.button ~attr:(VA.on_click (fun _ ->
          run_action (Data.Action.AddRule Data.Rule.empty)
        )) [VN.text "➕"]
    ] in
  let%sub regenerate_button =
    let%sub generate_timeline =
      let%arr state and run_action in
      let%bind.Effect res = Actions.solve (Data.State.to_raw state) in
      match res with
      | Ok model ->
        run_action (Data.Action.UpdateTimelines model)
      | Error error ->
        run_action (Data.Action.AddError (Error.to_string_hum error)) in
    let%arr generate_timeline in
    VN.button
      ~attr:VA.(class_ "timeline-regenerate" @
                on_click (fun _ -> generate_timeline)) [VN.text "↻"] in

  let%sub timeline_button_panel =
    let%sub save_model =
      let%arr state and run_action in
      let%bind.Effect res = Actions.save (Data.State.to_raw state) in
      match res with
      | Ok model ->
        Utils.download_file ~default_name:"saved-model.db" model;
        Ui_effect.Ignore
      | Error error ->
        run_action (Data.Action.AddError (Error.to_string_hum error)) in
    let%sub download_model =
      let%arr state in
      (fun () ->
        let res = Story_renderer_data.Clingo.make_clingo_input (Data.State.to_raw state) in
        Utils.download_string ~default_name:"query.lp" (Js_of_ocaml.Js.string res);
        Ui_effect.Ignore) in
    let%sub load_model =
      let%arr run_action in
      fun data ->
        let%bind.Effect res = Actions.load data in
        match res with
        | Ok model ->
          run_action (Data.Action.SetState model)
        | Error error ->
          run_action (Data.Action.AddError (Error.to_string_hum error)) in
    let%arr regenerate_button and save_model and load_model and download_model in
    VN.div ~attr:(VA.class_ "timeline-button-panel") [
      regenerate_button;
      VN.button ~attr:(
        VA.on_click (fun _ -> save_model)
      ) [
        VN.text "Save"
      ];
      VN.button ~attr:(VA.class_ "file-button") [
        VN.label ~attr:(VA.for_ "load") [VN.text "Load"];
        VN.input ~attr:VA.(VA.on_change (fun ev txt ->
            print_endline ("on change occurred: " ^ txt);
            let open Js_of_ocaml in
            let (let+) x f = match x with
              | None -> Ui_effect.Ignore
              | Some v -> f v in
            let+ target = Js.Opt.to_option ev##.target in
            let (fl : File.fileList Js.t) = Js.Unsafe.get target "files" in
            let+ file = Js.Opt.to_option (fl##item 0) in
            load_model (Js.Unsafe.coerce file)
          ) @ VA.(VA.id "load" @ VA.type_ "file")) [];
      ];
      VN.button ~attr:(
        VA.on_click (fun _ -> download_model ())
      ) [
        VN.text "Download"
      ];
    ] in
    
  let%arr state
  and options_panel
  and fixed_events_grid
  and property_grid
  and timeline_contents
  and rules_list
  and rules_add_button
  and error_panel
  and timeline_button_panel in
  VN.(div ~attr:(VA.classes ["panel"; "wrapped-panel"]) [
      div ~attr:(VA.classes ["panel"; "vertical-panel"]) [
        div ~attr:(VA.classes ["panel-title"]) [
          h2 [text "Timeline"];
          options_panel;
        ];
        div ~attr:(VA.classes ["timelines"]) [
          div ~attr:(VA.classes ["timeline"; "timeline-top-row"])
            ([
              VN.div
                ~attr:(VA.classes [
                    "timeline-top-row-header";
                    "timeline-header"
                  ]) []
            ] @ List.init (state.Data.State.max_time) ~f:timeline_event_marker);
          timeline_contents
        ];
        timeline_button_panel
      ];
      div ~attr:(VA.classes ["panel"]) [
        div ~attr:(VA.classes ["half-panel"]) [
          div ~attr:(VA.classes ["quarter-panel"; "vertical-panel"]) [
            div ~attr:(VA.classes ["panel-title"]) [
              h2 [text "Fixed Events"];
            ];
            div ~attr:(VA.class_ "panel-contents") [
              fixed_events_grid
            ]
          ];
          div ~attr:(VA.classes ["quarter-panel"; "vertical-panel"]) [
            div ~attr:(VA.classes ["panel-title"]) [
              h2 [text "Properties"];
            ];
            div ~attr:(VA.class_ "panel-contents") [
              property_grid
            ]
          ];
        ];
        div ~attr:(VA.classes ["half-panel"; "vertical-panel"]) [
          div ~attr:(VA.classes ["panel-title"]) [
            h2 [text "Rules"];
          ];
          div ~attr:(VA.class_ "panel-contents") (List.concat [
              rules_list;
              [rules_add_button]
            ])
        ];
      ];
      error_panel
    ])
