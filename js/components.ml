open! Core
open! Bonsai_web
open! Bonsai.Let_syntax

module VN = Vdom.Node
module VA = Vdom.Attr

let autocomplete_box ?(attr=VA.empty) ?(input_attr=VA.empty)
    set_text autocomplete_choices text =
  let%sub (autocomplete_box_visible, set_autocomplete_visible) =
    Bonsai.state [%here] ~default_model:false (module Bool) in
  let autocomplete_values =
    Value.map2 autocomplete_choices text
    ~f:(fun autocomplete_choices text ->
      Utils.StringMultiSet.matches autocomplete_choices ~pattern:text) in
  let%sub autocomplete_view =
    Utils.mapM autocomplete_values ~f:(fun _ value ->
        let%arr value
        and set_text
        and set_autocomplete_visible in
        VN.div ~attr:(VA.on_click (fun _ ->
            Ui_effect.Many [
              set_text value;
              set_autocomplete_visible false;
              Effect.Prevent_default
            ]
          )) [
          VN.strong [ VN.text value ]
        ]
      ) in
  let%sub autocomplete_values =
    if%sub autocomplete_box_visible
    then
      let%arr autocomplete_view in
      autocomplete_view
    else Computation.return [] in
  let%arr set_autocomplete_visible
  and set_text
  and text
  and autocomplete_values in
  VN.span ~attr [
    VN.input ~attr:VA.(
        value_prop text @
        create "size" (Int.to_string @@ String.length text) @
        type_ "text" @
        (* on_blur (fun _ -> set_autocomplete_visible false) @ *)
        on_focus (fun _ ->
            set_autocomplete_visible true) @
        on_change (fun _ text ->
            Ui_effect.Many [
              set_text text;
              set_autocomplete_visible false;
              Effect.Prevent_default
            ]
          ) @
        on_input (fun _ text -> set_text text) @
        input_attr
      ) [];
    VN.div ~attr:VA.(
        class_ "autocomplete-box"
      ) autocomplete_values
  ]
