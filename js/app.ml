[@@@warning "-32-27-37"]
open! Core
open! Bonsai_web
open! Bonsai.Let_syntax

module StringMultiSet = Utils.StringMultiSet
module StringMap = Map.Make (String)
module IntMap = Map.Make (Int)
module StringSet = Set.Make (String)

let apply_action :
  inject:('action -> unit Ui_effect.t) ->
  schedule_event:(unit Ui_effect.t -> unit) ->
  'model -> 'action -> 'model =
  fun ~inject:_ ~schedule_event:_ model action ->
  match (action: Data.Action.t) with
  | UpdateTimelines data -> Data.State.update_timelines data model
  | AddError error -> Data.State.add_error error model
  | RemoveError index -> Data.State.remove_error ~index model
  | SetState raw_model -> Data.State.of_raw model raw_model
  | SetMaxTime time ->
    begin match Int.of_string time with
      | time -> Data.State.update_max_time ~time model
      | exception _ -> model
    end
  | RemoveFixedClause index ->
    Data.State.remove_fixed_clause ~index model
  | SetFixedClauseTime {index; time} ->
    begin match Int.of_string time with
      | time ->
        Data.State.update_fixed_clause_time ~index ~time model
      | exception _ -> model
    end
  | SetFixedClauseValue {index; clause} ->
    Data.State.update_fixed_clause_value ~index clause model
  | SetFixedClauseHappens {index; happens} ->
    Data.State.update_fixed_clause_happens ~index ~happens model
  | AddFixedClause {time;clause} ->
    begin match Int.of_string time with
      | time -> Data.State.add_fixed_clause ~time clause model
      | exception _ -> model
    end
  | RemoveProperty index ->
    Data.State.remove_property ~index model
  | AddProperty clause ->
    Data.State.add_property clause model
  | SetPropertyValue {index; clause} ->
    Data.State.update_property_value ~index clause model

  | AddRule rule ->
    Data.State.add_rule rule model
  | RemoveRule rule_index ->
    Data.State.remove_rule ~rule_index model

  | SetRuleHeadClause { index; clause } ->
    Data.State.set_rule_head_clause ~rule_index:index clause model

  | AddRuleProperty { index; clause } ->
    Data.State.add_rule_property ~rule_index:index clause model

  | UpdateRuleNthProperty { index; property_index; clause } ->
    Data.State.update_rule_nth_property
      ~rule_index:index ~index:property_index clause model
  | RemoveRuleNthProperty { index; property_index } ->
    Data.State.remove_rule_nth_property
      ~rule_index:index ~index:property_index model
  | AddRuleInitiatedFluent { index; clause } ->
    Data.State.add_rule_initiated_fluent
      ~rule_index:index clause model
  | UpdateRuleNthInitiatedFluent { index; fluent_index; clause } ->
    Data.State.update_rule_nth_initiated_fluent
      ~rule_index:index ~index:fluent_index clause model

  | RemoveRuleNthInitiatedFluent { index; fluent_index } ->
    Data.State.remove_rule_nth_initiated_fluent
      ~rule_index:index ~index:fluent_index model
  | AddRuleTerminatedFluent { index; clause } ->
    Data.State.add_rule_terminated_fluent
      ~rule_index:index clause model

  | UpdateRuleNthTerminatedFluent { index; fluent_index; clause } ->
    Data.State.update_rule_nth_terminated_fluent
      ~rule_index:index ~index:fluent_index clause model

  | RemoveRuleNthTerminatedFluent { index; fluent_index } ->
    Data.State.remove_rule_nth_terminated_fluent
      ~rule_index:index ~index:fluent_index model
  | AddRulePrecondition { index; time; negated; clause } ->
    Data.State.add_rule_precondition
      ~rule_index:index ~time ~negated clause model
  | UpdateRuleNthPreconditionValue
      { index; precondition_index; clause } ->
    Data.State.update_rule_nth_precondition_value
      ~rule_index:index ~index:precondition_index clause model
  | UpdateRuleNthPreconditionTime { index; precondition_index; time } ->
    begin match Int.of_string time with
      | time ->
        Data.State.update_rule_nth_precondition_time
          ~rule_index:index ~index:precondition_index time model
      | exception _ -> model
    end
  | UpdateRuleNthPreconditionNegated
      { index; precondition_index; negated } ->
    Data.State.update_rule_nth_precondition_negated
      ~rule_index:index ~index:precondition_index negated model
  | RemoveRuleNthPrecondition { index; precondition_index } ->
    Data.State.remove_rule_nth_precondition
      ~rule_index:index ~index:precondition_index model


let app =
  let%sub state, run_action =
    Bonsai.state_machine0 [%here]
      (module Data.State) (module Data.Action)
      ~default_model:(Data.State.default ())
      ~apply_action in
  let%sub view = Render.render_state run_action state in
  return @@ view

let (_: _ Start.Handle.t) =
  Start.start Start.Result_spec.just_the_view ~bind_to_element_with_id:"app"
    app

