# Story Renderer

This project implements a fun little tool to generate
stories/timelines of events given appropriate rules in the event
calculus. 

![demo of story-renderer](https://codeberg.org/gopiandcode/story-renderer/raw/branch/master/images/demo.gif)

Under the hood it is implemented as an OCaml wrapper over a clingo
answer-set-programming (ASP) solver.

## Installation

To build the project, first install dependencies:

```bash
opam install . --deps-only
```

Then build:

```bash
dune build
```

Finally, to run the server, use:

```bash
dune exec -- ./bin/main.exe
```

The cli interface is documented as follows:

```
NAME
       story-renderer

SYNOPSIS
       story-renderer [--debug] [--port=VAL] [OPTION]…

OPTIONS
       -D, --debug
           Whether to run in debug mode.

       -p VAL, --port=VAL
           Port to run server on, defaults to 8080.

```
