open Core
module Data = Story_renderer_data

let raw_clause_to_clause (Data.Mk (head, args)) =
  let args = List.map ~f:(function
      Mk (head, []) -> head
    | head ->
      Format.kasprintf failwith "expecting literal arguments, found %a"
        Data.pp_raw_clause head
  ) args in
  Data.App (head, args)


let find_matching_termination ~after ~clause ls =
  let rec loop acc after clause ls =
    match ls with
    | [] -> None
    | ((time, _) as h) :: t when time < after ->
      loop (h :: acc) after clause t
    | ((time, oclause) as h) :: t ->
      if Data.equal_clause clause oclause
      then Some (time, List.rev_append acc t)
      else loop (h :: acc) after clause t in
  match loop [] after clause ls with
  | None -> None, ls
  | Some (time,ls) -> Some time, ls


let extract_model clauses =
  let (initiated,terminated, happens) = 
    List.partition3_map ~f:(function
      | Data.Mk ("initiated", [Mk (time, []); clause]) ->
        `Fst (Int.of_string time, raw_clause_to_clause clause)
      | Mk ("terminated", [Mk (time, []); clause]) ->
        `Snd (Int.of_string time, raw_clause_to_clause clause)
      | Mk ("happens", [Mk (time,[]); clause]) ->
        `Trd (Int.of_string time, raw_clause_to_clause clause)
      | clause ->
        Format.kasprintf failwith "expecting initiated,terminated or \
                                   happens clause, found %a"
          Data.pp_raw_clause clause
    ) clauses in
  let initiated =
    List.sort ~compare:(fun (head,_) (head', _) -> Int.compare head head')
      initiated in
  let terminated =
    List.sort ~compare:(fun (head,_) (head', _) -> Int.compare head head')
      terminated in
  let happens =
    List.sort ~compare:(fun (head,_) (head', _) -> Int.compare head head')
      happens in
  let events, terminated =
    List.fold_left
      ~init:([],terminated)
      ~f:(fun (events, terminated) (time, clause) ->
        let end_time, terminated =
          find_matching_termination
            ~after:time ~clause terminated in
        let event =
          ({start=Some time; end_=end_time;clause}: Data.event) in
        (event :: events, terminated)
      ) initiated in
  let terminated = List.map terminated ~f:(fun (time,clause) ->
    ({end_=Some time; start=None; clause}: Data.event)
  ) in
  let model : Data.model = {
    intervals=events @ terminated;
    events=happens
  } in
  model


let solve query =
  match Clingo.solve Data.Clingo.(make_clingo_input query) with
  | Error (`Msg m) -> Error (`OSError m)
  | Ok (_, false) -> Error (`Unsatisfiable)
  | Ok (clauses, true) ->
    try
      Ok (extract_model clauses)
    with exn ->
      Error (`ResultError (Exn.to_string exn))

