module Data = Story_renderer_data

val solve:
  Data.problem_spec ->
  (Data.model,
   [> `OSError of string
   | `ResultError of string
   | `Unsatisfiable ]) result
