open Core
open Angstrom

let ident = take_while (fun c ->
  Char.is_alphanum c || Char.(c = '_') || Char.(c = '-')
)

let lparen = char '('
let rparen = char ')'
let comma = char ','
let ws = take_while Char.is_whitespace

let sat = string "SATISFIABLE"
let unsat = string "UNSATISFIABLE"

let token =
  fix (fun token -> 
    let* name = ident in
    let+ args  = option [] (
      lparen *> sep_by (ws *> comma) (ws *> token) <* rparen
    ) in
    Story_renderer_data.Mk (name, args)
  )

let result =
  (sat *> return true) <|>
  (unsat *> return false)

let token_list =
  fix @@ fun token_list ->
  (ws *> result >>| fun res -> ([], res)) <|>
  (lift2 (fun tk (acc, sat) -> (tk :: acc, sat))
     (ws *> token) (token_list)
  )

let parse s =
  parse_string token_list ~consume:Consume.Prefix s
  |> Result.map_error ~f:(fun v -> `Msg v)
