open Core
module Data = Story_renderer_data 

let sanitize_error res =
  Lwt_result.map_error
    (function
      |`Msg m -> `Msg m
      | #Caqti_error.t as err ->
        `Msg (Caqti_error.show err))
    res

let serialise (problem: Data.problem_spec) =
  let open Lwt_result.Let_syntax in
  let%bind db = Lwt.return @@ Bos.OS.File.tmp "story-renderer-%s.db" in
  let db_uri =
    Uri.of_string ("sqlite3://" ^ Fpath.to_string db) in
  let%bind () = Caqti_lwt.with_connection db_uri (fun db ->
    let%bind () = Sql.initialise db in
    let%bind _ =
      Lwt_list.map_s (fun rule ->
        Dream.log "adding %a\n" Data.pp_rule rule;
        Lwt.bind (Operations.insert_rule rule db) (fun res ->
          begin match res with
          | Ok _ -> ()
          | Error (#Caqti_error.t as err) ->
            Dream.log "error: %s" (Caqti_error.show err)
          | Error (`Msg m) ->
            Dream.log "error msg: %s" m
          end;

          Lwt.return res
        )
      ) problem.rules
      |> Lwt.map Result.all in


    let%bind _ =
      Operations.insert_scenario problem.scenario db in
    Lwt_result.return ()
    ) |> sanitize_error in
  let%bind data = 
    Lwt_io.with_file ~mode:Lwt_io.Input (Fpath.to_string db) (fun ic ->
        Lwt_io.read ic
        |> Lwt.map Result.return
      ) in
  Lwt_result.return data

let deserialise (data: string) =
  let open Lwt_result.Let_syntax in
  let%bind db = Lwt.return @@ Bos.OS.File.tmp "story-renderer-%s.db" in
  let%bind () = 
    Lwt_io.with_file ~mode:Lwt_io.Output (Fpath.to_string db) (fun oc ->
        Lwt_io.write oc data
        |> Lwt.map Result.return
      ) in
  let db_uri =
    Uri.of_string ("sqlite3://" ^ Fpath.to_string db) in
  let%bind problem_spec = Caqti_lwt.with_connection db_uri (fun db ->
      let%bind rules =
        Operations.collect_all_rules db in
      let%bind scenarios =
        Operations.collect_all_scenarios db in
      match scenarios with
      | scenario :: _ -> Lwt_result.return (Data.{rules;scenario;max_time=10})
      | _ -> Lwt.return_error (`Msg "no scenarios in db")
    ) |> sanitize_error in
  Lwt_result.return problem_spec
