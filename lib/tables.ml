
let clause_spec,
    Sql.[clause_id; clause_name; clause_arity] =
  Sql.declare_table ~name:"clause_spec"
    ~constraints:[Sql.Schema.table_unique [
      "clause_name"; "clause_arity"
    ]]
    Sql.Schema.[
      field ~constraints:[primary_key ~auto_increment:true ()] "clause_id" ~ty:Sql.Type.int;
      field ~constraints:[not_null ()] "clause_name" ~ty:Sql.Type.text;
      field ~constraints:[not_null ()] "clause_arity" ~ty:Sql.Type.int;
    ]

let clause_instance, Sql.[instance_id; instance_clause] =
  Sql.declare_table ~name:"clause_instance"
    Sql.Schema.[
      field ~constraints:[primary_key ~auto_increment:true ()] "instance_id" ~ty:Sql.Type.int;
      field ~constraints:[foreign_key ~table:clause_spec ~columns:[clause_id] () ] "clause_id" ~ty:Sql.Type.int;
    ]

let clause_data,
    Sql.[
      data_instance_id;
      data_arg_index;
      data_contents
    ] =
  Sql.declare_table ~name:"clause_data"
    ~constraints:[
      Sql.Schema.table_primary_key [
        "data_instance_id";
        "data_arg_index"
      ] ]
    Sql.Schema.[
      field ~constraints:[
        foreign_key
          ~table:clause_instance
          ~columns:[instance_id] ()
      ] "data_instance_id" ~ty:Sql.Type.int;
      field ~constraints:[not_null ()]
        "data_arg_index" ~ty:Sql.Type.int;
      field ~constraints:[not_null ()]
        "data_contents" ~ty:Sql.Type.text;
    ]

let rule_spec, Sql.[rule_spec_id; rule_head] =
  Sql.declare_table ~name:"rule_spec"
    Sql.Schema.[
      field ~constraints:[primary_key ()]
        "rule_spec_id" ~ty:Sql.Type.int;
      field ~constraints:[
        foreign_key
          ~table:clause_instance
          ~columns:[instance_id] ()
      ] "rule_head" ~ty:Sql.Type.int;
    ]

let rule_constraints,
    Sql.[
      rule_constraint_spec_id;
      rule_constraint_clause
    ] =
  Sql.declare_table ~name:"rule_constraints"
    ~constraints:[
      Sql.Schema.table_primary_key [
        "rule_constraint_spec_id";
        "rule_constraint_clause"
      ] ]
    Sql.Schema.[
      field ~constraints:[
        foreign_key
          ~table:rule_spec
          ~columns:[rule_spec_id] ()
      ] "rule_constraint_spec_id" ~ty:Sql.Type.int;
      field ~constraints:[
        foreign_key
          ~table:clause_instance
          ~columns:[instance_id] ()
      ] "rule_constraint_clause" ~ty:Sql.Type.int;
    ]
    
let rule_preconditions, Sql.[
  rule_precondition_spec_id;
  rule_precondition_time_offset;
  rule_precondition_is_negated;
  rule_precondition_clause
] =
  Sql.declare_table ~name:"rule_preconditions"
    ~constraints:[
      Sql.Schema.table_primary_key [
        "rule_precondition_spec_id";
        "rule_precondition_time_offset";
        "rule_precondition_is_negated";
        "rule_precondition_clause"
      ]
    ]
    Sql.Schema.[
      field ~constraints:[
        foreign_key
          ~table:rule_spec
          ~columns:[rule_spec_id] ()
      ] "rule_precondition_spec_id" ~ty:Sql.Type.int;
      field ~constraints:[not_null ()]
       "rule_precondition_time_offset" ~ty:Sql.Type.int;
      field ~constraints:[not_null ()]
       "rule_precondition_is_negated" ~ty:Sql.Type.bool;
      field ~constraints:[
        foreign_key
          ~table:clause_instance
          ~columns:[instance_id] ()
      ] "rule_precondition_clause" ~ty:Sql.Type.int;
    ]

let rule_fluents,
    Sql.[
      rule_fluent_spec_id;
      rule_fluent_initiated;
      rule_fluent_clause
    ] =
  Sql.declare_table ~name:"rule_fluents"
    ~constraints:[ Sql.Schema.table_primary_key [
      "rule_fluent_spec_id";
      "rule_fluent_initiated";
      "rule_fluent_clause"
    ] ]
    Sql.Schema.[
      field ~constraints:[
        foreign_key
          ~table:rule_spec
          ~columns:[rule_spec_id] ()
      ] "rule_fluent_spec_id" ~ty:Sql.Type.int;
      field ~constraints:[not_null ()]
        "rule_fluent_initiated" ~ty:Sql.Type.bool;
      field ~constraints:[
        foreign_key
          ~table:clause_instance
          ~columns:[instance_id] ()
      ] "rule_fluent_clause" ~ty:Sql.Type.int;
    ]

let scenarios, Sql.[scenario_id; scenario_name] =
  Sql.declare_table ~name:"scenarios"
    Sql.Schema.[
      field ~constraints:[primary_key ()]
        "scenario_id" ~ty:Sql.Type.int;
      field ~constraints:[not_null ()]
        "scenario_name" ~ty:Sql.Type.text;
    ]

let constant_temporal_clauses, Sql.[
      constant_temporal_time;
      constant_temporal_scene;
      constant_temporal_clause
    ] =
  Sql.declare_table ~name:"constant_temporal_clauses"
    Sql.Schema.[
      field ~constraints:[not_null ()]
        "constant_temporal_time" ~ty:Sql.Type.int;
      field ~constraints:[
        foreign_key
          ~table:scenarios
          ~columns:[scenario_id] ()
      ] "constant_temporal_scene" ~ty:Sql.Type.int;
      field ~constraints:[
        foreign_key
          ~table:clause_instance
          ~columns:[instance_id] ()
      ] "constant_temporal_clause" ~ty:Sql.Type.int;
    ]

let constant_clauses, Sql.[
  constant_scene;
  constant_clause
] =
  Sql.declare_table ~name:"constant_clauses"
    Sql.Schema.[
      field ~constraints:[
        foreign_key
          ~table:scenarios
          ~columns:[scenario_id] ()
      ] "constant_scene" ~ty:Sql.Type.int;
      field ~constraints:[
        foreign_key
          ~table:clause_instance
          ~columns:[instance_id] ()
      ] "constant_clause" ~ty:Sql.Type.int;
    ]
