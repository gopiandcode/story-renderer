open Core
module Data = Story_renderer_data

let sanitize_error res =
  Lwt_result.map_error
    (fun err ->
       `Msg (Caqti_error.show err))
    res

include (struct
  type +'a link =  int
  let make_clause i = i
  let make_rule i = i
  let make_scenario i = i
end : sig
           type +'a link = private int
           val make_clause : int -> Data.clause link
           val make_rule: int -> Data.rule link
           val make_scenario: int -> Data.scenario link
         end)

let find_clause_spec clause db =
  let open Lwt_result.Let_syntax in
  let lookup_clause_spec
        (App (fn, args): Data.clause) db =
    let arg_count = List.length args in
    Sql.select ~from:Tables.clause_spec [Tables.clause_id]
    |> Sql.filter Sql.Expr.(Tables.clause_name = s fn &&
                            Tables.clause_arity =
                            i arg_count)
    |> Sql.Request.make_zero_or_one
    |> Sql.find_opt db
    |> sanitize_error in
  let ensure_clause_spec (App (fn, args)
                          : Data.clause) db =
    let arg_count = List.length args in
    let%bind () =
      Sql.insert ~table:Tables.clause_spec
        ~values:Sql.Expr.[
          Tables.clause_name := s fn;
          Tables.clause_arity := i arg_count;
        ]
      |> Sql.on_err `IGNORE
      |> Sql.Request.make_zero
      |> Sql.exec db
      |> sanitize_error in
    let%bind (id, _) =
      Sql.select Tables.[clause_id] ~from:Tables.clause_spec
      |> Sql.filter Sql.Expr.(Tables.clause_name = s fn &&
                              Tables.clause_arity = i arg_count)
      |> Sql.Request.make_one
      |> Sql.find db
      |> sanitize_error in
    Lwt_result.return id in
  let%bind id = lookup_clause_spec clause db in
  match id with
  | None ->
    ensure_clause_spec clause db
  | Some (id, _) ->
    Lwt_result.return id

let resolve_instance (id: Data.clause link) db =
  let open Lwt_result.Let_syntax in
  let%bind (clause_id, _) =
    Sql.select [Tables.instance_clause] ~from:Tables.clause_instance
    |> Sql.filter Sql.Expr.(Tables.instance_id = i (id :> int))
    |> Sql.Request.make_one
    |> Sql.find db
    |> sanitize_error in
  let%bind (f, (clause_arity, ())) =
    Sql.select [Tables.clause_name; Tables.clause_arity]
      ~from:Tables.clause_spec
    |> Sql.filter Sql.Expr.(Tables.clause_id = i clause_id)
    |> Sql.Request.make_one
    |> Sql.find db
    |> sanitize_error in
  let%bind args =
    Sql.select [Tables.data_contents] ~from:Tables.clause_data
    |> Sql.filter Sql.Expr.(Tables.data_instance_id = i (id :> int))
    |> Sql.order_by ~direction:`ASC Tables.data_arg_index
    |> Sql.Request.make_many
    |> Sql.collect_list db
    |> sanitize_error in
  let count = ref 0 in
  let args = List.map ~f:(fun (arg, ()) -> incr count; arg) args in
  if !count <> clause_arity
  then Lwt.return_error (`Msg (Format.sprintf
                                 "retrieved clause arity %d did not match expected one %d"
                                 !count clause_arity))
  else Lwt.return_ok (Data.App (f, args))


let ensure_clause_instance clause db =
  let open Lwt_result.Let_syntax in
  let insert_clause_instance (App (_, args) as clause : Data.clause) db =
    let%bind clause_spec = find_clause_spec clause db in
    let%bind () =
      Sql.insert ~table:Tables.clause_instance
        ~values:Sql.Expr.[Tables.instance_clause := i clause_spec]
      |> Sql.Request.make_zero
      |> Sql.exec db
      |> sanitize_error in
    let%bind (id, ()) =
      Sql.select Sql.Expr.[max Tables.instance_id]
        ~from:Tables.clause_instance
      |> Sql.filter Sql.Expr.(Tables.instance_clause = i clause_spec)
      |> Sql.Request.make_one
      |> Sql.find db
      |> sanitize_error in
    let%bind () =
      Lwt_list.mapi_s (fun ind arg ->
        Sql.insert ~table:Tables.clause_data
          ~values:Sql.Expr.[
            Tables.data_instance_id := i id;
            Tables.data_arg_index := i ind;
            Tables.data_contents := s arg
          ]
        |> Sql.on_err `ROLLBACK
        |> Sql.Request.make_zero
        |> Sql.exec db
      ) args
      |> Lwt.map (Result.all_unit)
      |> sanitize_error in
    Lwt_result.return (make_clause id) in

  let find_clause_instance (App (fn,args): Data.clause) db =
    let no_args = List.length args in
    let args = String.concat ~sep:"," args in
    let%bind res =
      let instance_data_query, (data_inst_id, data_args_list) =
        let args_list, args_list_ref =
          Sql.Expr.(
            group_concat
              ~sep_by:(s_stat ",") Tables.data_contents
            |> as_ ~name:"args_list") in
        let instance_id, instance_id_ref =
          Sql.Expr.as_ ~name:"inst_id"
            Tables.data_instance_id in
        let query = 
          Sql.select [instance_id; args_list]
            ~from:Tables.clause_data
          |> Sql.group_by [instance_id_ref]
          |> Sql.order_by ~direction:`ASC Tables.data_arg_index
        in
        (query, (instance_id_ref, args_list_ref)) in
      let clause_spec_query, (clause_spec_id, clause_spec_name) =
        let clause_id, clause_id_ref =
          Tables.clause_id
          |> Sql.Expr.as_ ~name:"cls_id" in
        let clause_name, clause_name_ref =
          Tables.clause_name
          |> Sql.Expr.as_ ~name:"cls_name" in
        let query = 
          Sql.select [clause_id; clause_name]
            ~from:Tables.clause_spec
          |> Sql.filter Sql.Expr.(clause_name_ref = s fn)
          |> Sql.filter Sql.Expr.(Tables.clause_arity = i no_args) in
        (query, (clause_id_ref, clause_name_ref)) in
      Sql.select [Tables.instance_id] ~from:Tables.clause_instance
      |> Sql.join clause_spec_query ~on:Sql.Expr.(Tables.instance_clause = clause_spec_id) 
      |> Sql.join instance_data_query ~on:Sql.Expr.(Tables.instance_id = data_inst_id)
      |> Sql.filter Sql.Expr.(data_args_list = s args)
      |> Sql.filter Sql.Expr.(clause_spec_name = s fn)
      |> Sql.Request.make_zero_or_one
      |> Sql.find_opt db
      |> sanitize_error in
    match res with
    | None -> Lwt_result.return None
    | Some (inst_id, ()) ->
      Lwt_result.return (Some (make_clause inst_id)) in
  let%bind res = find_clause_instance clause db in
  match res with
  | Some res -> Lwt_result.return res
  | None ->
    insert_clause_instance clause db

let insert_rule ({
  head;
  constraints;
  preconditions;
  initiated_fluents;
  terminated_fluents
}: Data.rule) db =
  let open Lwt_result.Let_syntax in
  let%bind head_id = ensure_clause_instance head db in
  let%bind (next_id, ()) =
    Sql.select Sql.Expr.[coalesce [max Tables.rule_spec_id; i_stat 0] + i_stat 1]
      ~from:Tables.rule_spec
    |> Sql.Request.make_one
    |> Sql.find db
    |> sanitize_error in
  let%bind () =
    Sql.insert ~table:Tables.rule_spec ~values:Sql.Expr.[
      Tables.rule_spec_id := i next_id;
      Tables.rule_head := i (head_id :> int);
    ] |> Sql.Request.make_zero
    |> Sql.exec db
    |> sanitize_error in
  let%bind () =
    Lwt_list.map_s (fun constraint_ ->
      let%bind constraint_id =
        ensure_clause_instance constraint_ db in
      Sql.insert ~table:Tables.rule_constraints
        ~values:Sql.Expr.[
          Tables.rule_constraint_spec_id := i next_id;
          Tables.rule_constraint_clause := i (constraint_id :> int)
        ]
      |> Sql.Request.make_zero
      |> Sql.exec db
      |> sanitize_error
    ) constraints
    |> Lwt.map Result.all_unit in
  let%bind () =
    Lwt_list.map_s (fun ({ offset; negated; clause }: Data.temporal_property) ->
      let%bind clause_id =
        ensure_clause_instance clause db in
      Sql.insert ~table:Tables.rule_preconditions
        ~values:Sql.Expr.[
          Tables.rule_precondition_spec_id := i next_id;
          Tables.rule_precondition_time_offset := i offset;
          Tables.rule_precondition_is_negated := bl negated;
          Tables.rule_precondition_clause := i (clause_id :> int);
        ]
      |> Sql.Request.make_zero
      |> Sql.exec db
      |> sanitize_error
    ) preconditions
    |> Lwt.map Result.all_unit in
  let%bind () =
    Lwt_list.map_s (fun clause ->
      let%bind clause_id =
        ensure_clause_instance clause db in
      Sql.insert ~table:Tables.rule_fluents
        ~values:Sql.Expr.[
          Tables.rule_fluent_spec_id := i next_id;
          Tables.rule_fluent_initiated := bl true;
          Tables.rule_fluent_clause := i (clause_id :> int);
        ]
      |> Sql.Request.make_zero
      |> Sql.exec db
      |> sanitize_error
    ) initiated_fluents
    |> Lwt.map Result.all_unit in
  let%bind () =
    Lwt_list.map_s (fun clause ->
      let%bind clause_id =
        ensure_clause_instance clause db in
      Sql.insert ~table:Tables.rule_fluents
        ~values:Sql.Expr.[
          Tables.rule_fluent_spec_id := i next_id;
          Tables.rule_fluent_initiated := bl false;
          Tables.rule_fluent_clause := i (clause_id :> int);
        ]
      |> Sql.Request.make_zero
      |> Sql.exec db
      |> sanitize_error
    ) terminated_fluents
    |> Lwt.map Result.all_unit in
  Lwt_result.return (make_rule next_id)

let resolve_rule (rule: Data.rule link) db =
  let open Lwt_result.Let_syntax in
  let%bind (head, ()) =
    Sql.select ~from:Tables.rule_spec [Tables.rule_head]
    |> Sql.filter Sql.Expr.(Tables.rule_spec_id = i (rule :> int))
    |> Sql.Request.make_one
    |> Sql.find db
    |> sanitize_error in
  let%bind head = resolve_instance (make_clause head) db in
  let%bind constraints =
    Sql.select ~from:Tables.rule_constraints
      [Tables.rule_constraint_clause]
    |> Sql.filter Sql.Expr.(Tables.rule_constraint_spec_id = i (rule :> int))
    |> Sql.Request.make_many
    |> Sql.collect_list db
    |> sanitize_error in
  let%bind constraints = 
    Lwt_list.map_s (fun (constraint_, ()) ->
      resolve_instance (make_clause constraint_) db
    ) constraints
    |> Lwt.map Result.all in
  let%bind preconditions =
    Sql.select [
      Tables.rule_precondition_is_negated;
      Tables.rule_precondition_time_offset;
      Tables.rule_precondition_clause;
    ] ~from:Tables.rule_preconditions
    |> Sql.filter Sql.Expr.(Tables.rule_precondition_spec_id = i (rule :> int))
    |> Sql.Request.make_many
    |> Sql.collect_list db
    |> sanitize_error in
  let%bind preconditions = 
    Lwt_list.map_s (fun (is_negatated, (time_offset, (clause, ()))) ->
      let%bind clause = resolve_instance (make_clause clause) db in
      Lwt_result.return ({
        offset=time_offset;
        negated=is_negatated;
        clause }: Data.temporal_property)
    ) preconditions
    |> Lwt.map Result.all in

  let%bind fluents =
    Sql.select [
      Tables.rule_fluent_initiated;
      Tables.rule_fluent_clause
    ] ~from:Tables.rule_fluents
    |> Sql.filter Sql.Expr.(Tables.rule_fluent_spec_id = i (rule :> int))
    |> Sql.Request.make_many
    |> Sql.collect_list db
    |> sanitize_error in
  let%bind fluents = 
    Lwt_list.map_s (fun (initiated, (clause, ())) ->
      let%bind clause = resolve_instance (make_clause clause) db in
      Lwt_result.return (initiated,clause)
    ) fluents
    |> Lwt.map Result.all in

  let initiated_fluents, terminated_fluents =
    List.partition_map fluents ~f:(fun (initiated, clause) ->
      if initiated
      then Either.First clause
      else Either.Second clause
    ) in
  Lwt_result.return ({
    head;
    constraints;
    preconditions;
    initiated_fluents;
    terminated_fluents
  }: Data.rule)

let collect_all_rules db =
  let open Lwt_result.Let_syntax in
  let%bind ids =
    Sql.select [Tables.rule_spec_id]
      ~from:Tables.rule_spec
    |> Sql.Request.make_many
    |> Sql.collect_list db
    |> sanitize_error in
  let%bind rules =
    Lwt_list.map_s (fun (id, ()) ->
        resolve_rule (make_rule id) db
      ) ids
    |> Lwt.map Result.all in
  Lwt_result.return rules

let remove_constraint (rule: Data.rule link) constraint_ db =
  let open Lwt_result.Let_syntax in
  let%bind constraint_ =
    ensure_clause_instance constraint_ db in
  Sql.delete ~from:Tables.rule_constraints
  |> Sql.filter Sql.Expr.(
    Tables.rule_constraint_spec_id = i (rule :> int) &&
    Tables.rule_constraint_clause = i (constraint_ :> int)
  )
  |> Sql.Request.make_zero
  |> Sql.exec db  
  |> sanitize_error

let remove_precondition (rule: Data.rule link) ({ offset; negated; clause }: Data.temporal_property) db =
  let open Lwt_result.Let_syntax in
  let%bind clause = ensure_clause_instance clause db in
  Sql.delete ~from:Tables.rule_preconditions
  |> Sql.filter Sql.Expr.(Tables.rule_precondition_spec_id = i (rule :> int))
  |> Sql.filter Sql.Expr.(Tables.rule_precondition_clause = i (clause :> int))
  |> Sql.filter Sql.Expr.(Tables.rule_precondition_time_offset = i offset)
  |> Sql.filter Sql.Expr.(Tables.rule_precondition_is_negated = bl negated)
  |> Sql.Request.make_zero
  |> Sql.exec db  
  |> sanitize_error

let remove_initiated_fluent (rule: Data.rule link) clause db =
  let open Lwt_result.Let_syntax in
  let%bind clause = ensure_clause_instance clause db in
  Sql.delete ~from:Tables.rule_fluents
  |> Sql.filter Sql.Expr.(Tables.rule_fluent_spec_id = i (rule :> int))
  |> Sql.filter Sql.Expr.(Tables.rule_fluent_initiated = true_)
  |> Sql.filter Sql.Expr.(Tables.rule_fluent_clause = i (clause :> int))
  |> Sql.Request.make_zero
  |> Sql.exec db  
  |> sanitize_error

let remove_terminated_fluent (rule: Data.rule link) clause db =
  let open Lwt_result.Let_syntax in
  let%bind clause = ensure_clause_instance clause db in
  Sql.delete ~from:Tables.rule_fluents
  |> Sql.filter Sql.Expr.(Tables.rule_fluent_spec_id = i (rule :> int))
  |> Sql.filter Sql.Expr.(Tables.rule_fluent_initiated = false_)
  |> Sql.filter Sql.Expr.(Tables.rule_fluent_clause = i (clause :> int))
  |> Sql.Request.make_zero
  |> Sql.exec db  
  |> sanitize_error

let remove_rule (rule: Data.rule link) db =
  let open Lwt_result.Let_syntax in
  let%bind () = 
    Sql.delete ~from:Tables.rule_constraints
    |> Sql.filter Sql.Expr.(Tables.rule_constraint_spec_id = i (rule :> int))
    |> Sql.Request.make_zero
    |> Sql.exec db
    |> sanitize_error in
  let%bind () = 
    Sql.delete ~from:Tables.rule_preconditions
    |> Sql.filter Sql.Expr.(Tables.rule_precondition_spec_id = i (rule :> int))
    |> Sql.Request.make_zero
    |> Sql.exec db
    |> sanitize_error in
  let%bind () = 
    Sql.delete ~from:Tables.rule_fluents
    |> Sql.filter Sql.Expr.(Tables.rule_fluent_spec_id = i (rule :> int))
    |> Sql.Request.make_zero
    |> Sql.exec db
    |> sanitize_error in
  let%bind () = 
    Sql.delete ~from:Tables.rule_spec
    |> Sql.filter Sql.Expr.(Tables.rule_spec_id = i (rule :> int))
    |> Sql.Request.make_zero
    |> Sql.exec db
    |> sanitize_error in
  Lwt_result.return ()

let insert_scenario ({ name; temporal_clauses; constant_clauses }: Data.scenario) db =
  let open Lwt_result.Let_syntax in
  let%bind (next_id, ()) =
    Sql.select Sql.Expr.[coalesce [max Tables.scenario_id; i_stat 0] + i_stat 1]
      ~from:Tables.scenarios
    |> Sql.Request.make_one
    |> Sql.find db 
    |> sanitize_error in
  let%bind () =
    Sql.insert ~table:Tables.scenarios ~values:Sql.Expr.[
      Tables.scenario_id := i next_id;
      Tables.scenario_name := s name;
    ] |> Sql.Request.make_zero
    |> Sql.exec db
    |> sanitize_error in
  let%bind () =
    Lwt_list.map_s (fun (offset, _, clause) -> (* TODO: serialise happens *)
      let%bind clause = ensure_clause_instance clause db in
      Sql.insert ~table:Tables.constant_temporal_clauses
        ~values:Sql.Expr.[
          Tables.constant_temporal_time := i offset;
          Tables.constant_temporal_scene := i next_id;
          Tables.constant_temporal_clause := i (clause :> int);
        ]
      |> Sql.Request.make_zero
      |> Sql.exec db
      |> sanitize_error) temporal_clauses
    |> Lwt.map Result.all_unit in
  let%bind () =
    Lwt_list.map_s (fun clause ->
      let%bind clause = ensure_clause_instance clause db in
      Sql.insert ~table:Tables.constant_clauses
        ~values:Sql.Expr.[
          Tables.constant_scene := i next_id;
          Tables.constant_clause := i (clause :> int);
        ]
      |> Sql.Request.make_zero
      |> Sql.exec db
      |> sanitize_error) constant_clauses
    |> Lwt.map Result.all_unit in

  Lwt_result.return (make_scenario next_id)

let resolve_scenario (scen_id: Data.scenario link) db =
  let open Lwt_result.Let_syntax in
  let%bind (name, ()) =
    Sql.select [Tables.scenario_name]
      ~from:Tables.scenarios
    |> Sql.filter Sql.Expr.(Tables.scenario_id = i (scen_id :> int))
    |> Sql.Request.make_one
    |> Sql.find db
    |> sanitize_error in

  let%bind temporal_clauses =
    let%bind temporal_clauses =
      Sql.select [
        Tables.constant_temporal_time;
        Tables.constant_temporal_clause
      ] ~from:Tables.constant_temporal_clauses
      |> Sql.filter Sql.Expr.(
        Tables.constant_temporal_scene = i (scen_id :> int)
      )
      |> Sql.Request.make_many
      |> Sql.collect_list db
      |> sanitize_error in
    Lwt_list.map_s (fun (offset, (clause, ())) ->
      let%bind clause = resolve_instance (make_clause clause) db in
      Lwt_result.return (offset, false, clause)) temporal_clauses
    |> Lwt.map Result.all in
  let%bind constant_clauses =
    let%bind constant_clauses =
      Sql.select [
        Tables.constant_clause
      ] ~from:Tables.constant_clauses
      |> Sql.filter Sql.Expr.(Tables.constant_scene = i (scen_id :> int))
      |> Sql.Request.make_many
      |> Sql.collect_list db
      |> sanitize_error in
    Lwt_list.map_s (fun (clause, ()) ->
      resolve_instance (make_clause clause) db) constant_clauses
    |> Lwt.map Result.all in
  Lwt_result.return ({name;temporal_clauses;constant_clauses}: Data.scenario)

let collect_all_scenarios db =
  let open Lwt_result.Let_syntax in
  let%bind ids =
    Sql.select [Tables.scenario_id]
      ~from:Tables.scenarios
    |> Sql.Request.make_many
    |> Sql.collect_list db
    |> sanitize_error in
  let%bind scenarios =
    Lwt_list.map_s (fun (id, ()) ->
        resolve_scenario (make_scenario id) db
      ) ids
    |> Lwt.map Result.all in
  Lwt_result.return scenarios

let remove_scenario (scen_id: Data.scenario link) db =
  let open Lwt_result.Let_syntax in
  let%bind () =
    Sql.delete ~from:Tables.constant_temporal_clauses
    |> Sql.filter Sql.Expr.(
      Tables.constant_temporal_scene = i (scen_id :> int)
    )
    |> Sql.Request.make_zero
    |> Sql.exec db
    |> sanitize_error in
  let%bind () =
    Sql.delete ~from:Tables.constant_clauses
    |> Sql.filter Sql.Expr.(Tables.constant_scene = i (scen_id :> int))
    |> Sql.Request.make_zero
    |> Sql.exec db
    |> sanitize_error in
  let%bind () =
    Sql.delete ~from:Tables.scenarios
    |> Sql.filter Sql.Expr.(Tables.scenario_id = i (scen_id :> int))
    |> Sql.Request.make_zero
    |> Sql.exec db
    |> sanitize_error in
  Lwt_result.return ()
