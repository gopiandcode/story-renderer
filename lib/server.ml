open Core
module Sexp = Sexplib.Sexp

let log = lazy (Dream.sub_log "server")

let html ?status ?code ?headers s = Lwt_result.ok @@ Dream.html ?status ?code ?headers s
let binary ?status ?code ?headers s =
  let headers = Option.value ~default:[] headers
                |> List.cons ("Content-Type", "application/x-binary") in
  Lwt_result.ok @@
  Dream.respond ?status ?code ~headers s

let sexp ?status ?code ?headers s =
  let headers = Option.value ~default:[] headers
                |> List.cons ("Content-Type", "text/plain") in
  Lwt_result.ok @@
  Dream.respond ?status ?code ~headers (Sexp.to_string_mach s)

let handle_error v = Lwt.bind v (function
  | Ok v -> Lwt.return v
  | Error (`Msg err) ->
    let headers = []
                |> List.cons ("Content-Type", "text/plain") in
    Dream.respond ~headers ~status:`Internal_Server_Error err)


let run ?(debug=false) port =
  Dream.run ~port:(Option.value ~default:8008 port) begin
    (if debug then Dream.logger else Fun.id) @@
    Dream.memory_sessions @@
    Dream.router [
      Dream.scope "/static" [] [
        Dream.get "/js/**" @@ Dream.static "_build/default/js";
        Dream.get "/styles/**" @@ Dream.static "_build/default/styles";
      ];
      Dream.scope "/api" [] [
        Dream.post "/save" (fun req ->
            let open Lwt_result.Let_syntax in
            handle_error begin
              let%bind problem =
                Lwt_result.ok (Dream.body req) in
              let%bind problem =
                Result.try_with (fun () ->
                    [%of_sexp: Story_renderer_data.problem_spec]
                      (Sexp.of_string problem))
                |> Result.map_error ~f:(fun err ->
                    `Msg ("Invalid data: " ^ Exn.to_string err))
                |> Lwt.return in
              let%bind data = Serialisation.serialise problem in
              binary data
            end
          );
        Dream.post "/load" (fun req ->
            let open Lwt_result.Let_syntax in
            handle_error begin
              let%bind data =
                Lwt_result.ok (Dream.body req) in
              Out_channel.with_file "output.db" ~f:(fun oc ->
                Out_channel.output_string oc data);
              let%bind problem = Serialisation.deserialise data in
              sexp ([%sexp_of: Story_renderer_data.problem_spec]
                      problem)
            end
          );
        Dream.post "/solve" (fun req ->
            let open Lwt_result.Let_syntax in
            handle_error begin
              let%bind problem =
                Lwt_result.ok (Dream.body req) in
              let%bind problem =
                Result.try_with (fun () ->
                    [%of_sexp: Story_renderer_data.problem_spec]
                      (Sexp.of_string problem))
                |> Result.map_error ~f:(fun err ->
                    `Msg ("Invalid data: " ^ Exn.to_string err))
                |> Lwt.return in
              let%bind data =
                Event_calculus.solve problem
                |> Result.map_error ~f:(function
                    | `Unsatisfiable -> "Unsatisfiable configuration"
                    | `OSError err -> err
                    | `ResultError err -> err
                  )
                |> Lwt.return_ok in
              sexp ([%sexp_of: (Story_renderer_data.model, string) Result.t]
                      data)
            end
          )
      ];

      Dream.get "/" (fun _req ->
          Dream.html {html|
          <!doctype html>
          <html lang="en">
             <head>
                 <meta charset="utf-8">
                 <meta name="viewport" content="width=device-width, initial-scale=1">
<title>Story Renderer</title>
   <link rel="stylesheet" href="static/styles/style.css">
             </head>
             <body>
                <div id="app">
                </div>
                <script src="static/js/app.bc.js"></script>
             </body>
          </html>
        |html}
        );
    ]
  end
