module Data = Story_renderer_data

type +'a link = private int

val resolve_instance :
  Data.clause link -> (module Caqti_lwt.CONNECTION) -> (Data.clause, [> `Msg of string ]) Lwt_result.t

val ensure_clause_instance :
  Data.clause -> (module Caqti_lwt.CONNECTION) -> (Data.clause link, [> `Msg of string ]) Lwt_result.t

val insert_rule :
  Data.rule -> (module Caqti_lwt.CONNECTION) -> (Data.rule link, [> `Msg of string ]) Lwt_result.t

val resolve_rule :
  Data.rule link -> (module Caqti_lwt.CONNECTION) -> (Data.rule, [> `Msg of string ]) Lwt_result.t

val collect_all_rules:
  (module Caqti_lwt.CONNECTION) ->
  (Data.rule list, [> `Msg of string ]) Lwt_result.t

val remove_constraint :
  Data.rule link -> Data.clause -> (module Caqti_lwt.CONNECTION) -> (unit, [> `Msg of string ]) Lwt_result.t

val remove_precondition :
  Data.rule link -> Data.temporal_property -> (module Caqti_lwt.CONNECTION) -> (unit, [> `Msg of string ]) Lwt_result.t

val remove_initiated_fluent :
  Data.rule link -> Data.clause -> (module Caqti_lwt.CONNECTION) -> (unit, [> `Msg of string ]) Lwt_result.t

val remove_terminated_fluent :
  Data.rule link -> Data.clause -> (module Caqti_lwt.CONNECTION) -> (unit, [> `Msg of string ]) Lwt_result.t

val remove_rule :
  Data.rule link -> (module Caqti_lwt.CONNECTION) -> (unit, [> `Msg of string ]) Lwt_result.t

val insert_scenario :
  Data.scenario -> (module Caqti_lwt.CONNECTION) -> (Data.scenario link, [> `Msg of string ]) Lwt_result.t

val resolve_scenario :
  Data.scenario link -> (module Caqti_lwt.CONNECTION) -> (Data.scenario, [> `Msg of string ]) Lwt_result.t

val collect_all_scenarios:
  (module Caqti_lwt.CONNECTION) ->
  (Data.scenario list, [> `Msg of string ]) Lwt_result.t

val remove_scenario :
  Data.scenario link -> (module Caqti_lwt.CONNECTION) -> (unit, [> `Msg of string ]) Lwt_result.t

