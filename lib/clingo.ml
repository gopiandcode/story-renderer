open Core

let clingo_bin = Bos.Cmd.v "clingo"

let solve input =
  let open Result.Let_syntax in
  let out = Bos.OS.Cmd.run_io
              ~err:Bos.OS.Cmd.err_stderr
              Bos.Cmd.(clingo_bin
                       % "--project"
                       % "--quiet=0,2,2"
                       % "--verbose=0")
              (Bos.OS.Cmd.in_string input) in
  let%bind (os, _) = (Bos.OS.Cmd.out_string out) in
  Parser.parse os
