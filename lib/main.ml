[@@@warning "-32-33"]
open Core
module Data = Story_renderer_data
module SR = Story_renderer
module SRO = SR.Operations

let ensure = function Ok v -> v | Error (`Msg err) -> failwith err

let raw_clause_to_clause (Data.Mk (head, args)) =
  let args = List.map ~f:(function
        Data.Mk (head, []) -> head
      | head ->
        Format.kasprintf failwith "expecting literal arguments, found %a"
          Data.pp_raw_clause head
    ) args in
  Data.App (head, args)

let find_matching_termination ~after ~clause ls =
  let rec loop acc after clause ls =
    match ls with
    | [] -> None
    | ((time, _) as h) :: t when time < after ->
      loop (h :: acc) after clause t
    | ((time, oclause) as h) :: t ->
      if Data.equal_clause clause oclause
      then Some (time, List.rev_append acc t)
      else loop (h :: acc) after clause t in
  match loop [] after clause ls with
  | None -> None, ls
  | Some (time,ls) -> Some time, ls

let handle_clauses clauses =
  let (initiated,terminated, happens) = 
    List.partition3_map ~f:Data.(function
        | Mk ("initiated", [Mk (time, []); clause]) ->
          `Fst (Int.of_string time, raw_clause_to_clause clause)
        | Mk ("terminated", [Mk (time, []); clause]) ->
          `Snd (Int.of_string time, raw_clause_to_clause clause)
        | Mk ("happens", [Mk (time,[]); clause]) ->
          `Trd (Int.of_string time, raw_clause_to_clause clause)
        | clause ->
          Format.kasprintf failwith "expecting initiated,terminated or \
                                     happens clause, found %a"
            Data.pp_raw_clause clause
      ) clauses in
  let initiated =
    List.sort ~compare:(fun (head,_) (head', _) -> Int.compare head head')
      initiated in
  let terminated =
    List.sort ~compare:(fun (head,_) (head', _) -> Int.compare head head')
      terminated in
  let happens =
    List.sort ~compare:(fun (head,_) (head', _) -> Int.compare head head')
      happens in

  let events, terminated =
    List.fold_left
      ~init:([],terminated)
      ~f:(fun (events, terminated) (time, clause) ->
          let end_time, terminated =
            find_matching_termination
              ~after:time ~clause terminated in
          let event =
            ({start=Some time; end_=end_time;clause}: Data.event) in
          (event :: events, terminated)
        ) initiated in
  let terminated = List.map terminated ~f:(fun (time,clause) ->
      ({end_=Some time; start=None; clause}: Data.event)
    ) in
  let model : Data.model = {
    intervals=events @ terminated;
    events=happens
  } in
  model

let _f () =
  let input = "
person(john).
person(bob).
place(london).
place(glasgow).

holds(0, at(john, london)).
holds(0, at(bob, glasgow)).

possible(T,moves(C,OP,NP)) :-
   holds(T, at(C,OP)), not holds(T,at(C,NP)),
   time(T), person(C), place(OP), place(NP).
initiates(T,moves(C,OP,NP),at(C,NP)) :-
    possible(T,moves(C,OP,NP)).
terminates(T,moves(C,OP,NP),at(C,OP)) :-
    possible(T,moves(C,OP,NP)).
" in
  let _result = (* Event_calculus.solve *) input in
  (* print_endline ("output is \"" ^ *)
  (*                ([%show: Story_renderer_data.raw_clause list * bool] *)
  (*                   result) *)
  (*                ^ "\"") *)
  ()
